﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MealPlanner2.Model;
using MealPlanner2.Model.Repositories;

namespace MealPlanner2.BaseLib
{
    public interface IAddWeeklyPlanView
    {
        void ShowViewModal(IMainController mainController, List<Recipe> allRecipes, WeeklyPlan plan, IRecipeRespository recipeRepo);
    }
}
