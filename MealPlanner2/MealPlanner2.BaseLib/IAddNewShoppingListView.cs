﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MealPlanner2.Model;

namespace MealPlanner2.BaseLib
{
    public interface IAddNewShoppingListView
    {
       string ListName { get; }
       List<Ingredient> ListIngredients { get; }
       bool ShowViewModal();

    }
}
