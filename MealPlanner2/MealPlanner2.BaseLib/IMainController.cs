﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MealPlanner2.Model;

namespace MealPlanner2.BaseLib
{
    public interface IMainController
    {
        
        void AddRecipe();
        void AddWeeklyPlan(DailyPlan[] dailyPlans, DateTime date, int id);
        void ShowAddWeeklyPlan();
        void ShowWeeklyPlan();
        void EditWeeklyPlan(WeeklyPlan plan);
        void DeleteWeeklyPlan(int id);
        void ShowRecipies();
        void viewDetailsRecipe(int id);
        void AddShoppingList();
        void ShowShoppingList();
        void EditShoppingList(ShoppingList list);
        void DeleteShoppingList(int id);
        void AddAllIngredientsToList(int id, string listName);
        void AddChosenIngredientsToList(List<Ingredient> ingredients, int id);
        void ShowAddChosenIngredients(int id, string idNameList);
        void LoginUser();
        void setLoggedUser(User user);
        User getLoggedUser();
        void logoutUser();

        void addRecipeNhibernate();


     


    }
}
