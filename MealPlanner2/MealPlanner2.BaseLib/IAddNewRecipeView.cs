﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MealPlanner2.Model;

namespace MealPlanner2.BaseLib
{
    public interface IAddNewRecipeView
    {
        bool ShowViewModal();

      

        string RecipeName { get;}
        string RecipeDescription { get; }
        string RecipeCategory { get; }
        List<Ingredient> RecipeIngredients { get;  }
        User RecipeOwner { get;  }
        decimal RecipePreparation { get; }
        decimal RecipePortions { get; }
       bool IsPublic { get; }
    }
}
