﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MealPlanner2.BaseLib
{
    public interface IAddNewIngredientView
    {
        bool ShowViewModal();

        string IngredientName { get; }
        string IngredientQuantity { get; }
    }
}
