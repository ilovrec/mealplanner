﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MealPlanner2.Model;

namespace MealPlanner2.BaseLib
{
    public interface IWindowsFormsFactory
    {
        //IAddUserView CreateAddNewUserView();

        // ILoginUserView CreateLoginUserView();

        IAddNewIngredientView CreateAddNewIngredientView();

        IAddNewRecipeView CreateAddNewRecipeView();

        IAddNewShoppingListView CreateAddNewShoppingListView();

        IShowAllRecipesView CreateShowAllRecipesView();

        IViewDetailsRecipe CreateViewDetailsRecipeView();

        IShowShoppingLists CreateShowAllShoppingListsView();

        IAddWeeklyPlanView CreateAddNewWeeklyPlanView();

        IShowWeeklyPlansView CreateShowAllWeeklyPlansView();
        ILoginUserView CreateLoginUserView();
        IChooseIngredientsView CreateChooseIngredientsView();
    }
}
