﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MealPlanner2.BaseLib
{
    public interface ILoginUserView
    {
        bool ShowViewModal();
        string Username { get; }
        string Password { get; }
    }
}
