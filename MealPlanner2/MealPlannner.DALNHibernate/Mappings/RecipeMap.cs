﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using MealPlanner2.Model;

namespace MealPlanner2.DALNHibernate.Mappings
{
    class RecipeMap: ClassMap<Recipe>
    {
        public RecipeMap()
        {
            Id(x => x.Id);
            Map(x => x.Name);
            Map(x => x.Category);
            Map(x => x.Description);
            Map(x => x.IsPublic);
            HasMany(x => x.Ingredients).Inverse().Cascade.All();


        }
    }

    
}
