﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using F
using MealPlanner2.Model;

namespace MealPlanner2.DALNHibernate.Mappings
{
    class IngredientMap: ClassMap<Ingredient>
    {
       public IngredientMap()
        {
            Id(x => x.Id);
            Map(x => x.Name);
            Map(x => x.Quantity);
            Map(x => x.MeasureUnit);
            References(x => x.Recipe);
        }

    }
   
}
