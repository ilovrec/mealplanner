﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MealPlanner2.Model;

namespace MealPlanner2.UnitTest
{
    [TestClass]
    public class IngredientTest
    {
        List<Ingredient> ingredients;

        [TestInitialize()]
        public void Init()
        {
            ingredients= new List<Ingredient>();
            ingredients.Add(new Ingredient("Fanta", 2, "L"));
            ingredients.Add(new Ingredient("Cola", 1, "L"));
            ingredients.Add(new Ingredient("Sir", 20, "dg"));
        }

        [TestMethod()]
        public void MsUnit_getIngredientNames()
        {
            String names = Ingredient.getIngredientsNames(ingredients);

            Assert.AreEqual("Fanta, Cola, Sir", names);
        }
    }
}
