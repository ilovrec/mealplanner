﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MealPlanner2.Model.Repositories;
using MealPlanner2.Model;

namespace MealPlanner2.Moq.DataProcessing.Tests
{
    public class DataProcessor
    {
        public static void MockAddRecipe(IRecipeRespository recipeRespository, IIngredientRepository ingredientRepository, Recipe recipe)
        {
            foreach(var ing in recipe.Ingredients)
            {
                ingredientRepository.addIngredient(ing);
            }

            recipeRespository.addRecipe(recipe);
        }

        public static void MockAddWeeklyPlan(IWeeklyPlanRepository weeklyPlanRepository, IDailyPlanRepository dailyPlanRepo, WeeklyPlan weeklyPlan)
        {
            foreach (var day in weeklyPlan.PlansArray)
                dailyPlanRepo.addDailyPlan(day);
            weeklyPlanRepository.addWeeklyPlan(weeklyPlan);
        }

        public static void MockAddAllIngredientsOfRecipe(IRecipeRespository recipeRespository, IShoppingListRepository shoppingListRepository, int idRecipe, int idList)
        {
            Recipe recipe = recipeRespository.getRecipeById(idRecipe);
            ShoppingList shoppingList = shoppingListRepository.getShoppingListById(idList);
            foreach (var ing in recipe.Ingredients)
            {
                shoppingList.Ingredients.Add(ing);
            }
            shoppingListRepository.editShoppingList(shoppingList);
        }

        public static void MockEditWeeklyPlan(IWeeklyPlanRepository weeklyPlanRepository, IDailyPlanRepository dailyPlanRepo, WeeklyPlan weeklyPlan, DailyPlan[] days)
        {
            WeeklyPlan plan = weeklyPlanRepository.getWeeklyPlanById(weeklyPlan.Id);
            
            for (int i = 0; i < 7; i++)
            {
                dailyPlanRepo.deleteDailyPlan(plan.PlansArray[i]);
                dailyPlanRepo.addDailyPlan(days[i]);
                plan.AddDailyPlan(days[i]);
            }
            plan.deleteDailyPlans();
            weeklyPlanRepository.editWeeklyPlan(weeklyPlan);
        }

       
    }
}
