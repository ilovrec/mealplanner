﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MealPlanner2.Model;
using MealPlanner2.Model.Repositories;
using Moq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MealPlanner2.Controllers;


namespace MealPlanner2.Moq.DataProcessing.Tests
{
    [TestClass]
    public class DataProcessingTests
    {
        

        [TestMethod]
        public void Moq_AddRecipe()
        {
            Ingredient ing = new Ingredient("Fanta", 2, "L");
            Recipe recipe = new Recipe("Recept", "opis", 35, 4, new List<Ingredient>() { ing }, "Salata", true, "Anonymous");
            var RecipeRepository = new Mock<IRecipeRespository>();
            var IngredientRepository = new Mock<IIngredientRepository>();

            RecipeRepository.Setup(x => x.addRecipe(recipe));
            IngredientRepository.Setup(x => x.addIngredient(ing));

            DataProcessor.MockAddRecipe(RecipeRepository.Object, IngredientRepository.Object, recipe);
            IngredientRepository.Verify(x => x.addIngredient(ing), Times.Once);
            RecipeRepository.Verify(x => x.addRecipe(recipe), Times.Once);
        }

        [TestMethod]
        public void Moq_AddWeeklyPlan()
        {
            Ingredient ing = new Ingredient("Fanta", 2, "L");
            Recipe recipe = new Recipe("Recept", "opis", 35, 4, new List<Ingredient>() { ing }, "Salata", true, "Anonymous");
            DailyPlan day = new DailyPlan();
            day.DayOfWeek = 0;
            day.AddBreakfast(recipe);
            WeeklyPlan week = new WeeklyPlan();
            for(int i=0; i<7; i++)
            {
                week.AddDailyPlan(day);
            }
            var DailyPlanRepository = new Mock<IDailyPlanRepository>();
            var WeeklyPlanRepository = new Mock<IWeeklyPlanRepository>();

            DailyPlanRepository.Setup(x => x.addDailyPlan(day));
            WeeklyPlanRepository.Setup(x => x.addWeeklyPlan(week));

            DataProcessor.MockAddWeeklyPlan(WeeklyPlanRepository.Object, DailyPlanRepository.Object, week);
            DailyPlanRepository.Verify(x => x.addDailyPlan(day), Times.Exactly(7));
            WeeklyPlanRepository.Verify(x => x.addWeeklyPlan(week), Times.Once);
        }

        [TestMethod]
        public void Moq_AddAllIngredientsOfRecipe()
        {
            Ingredient ing = new Ingredient("Fanta", 2, "L");
            Ingredient ing2 = new Ingredient("Cola", 2, "L");
            Recipe recipe = new Recipe("Recept", "opis", 35, 4, new List<Ingredient>() { ing, ing2 }, "Salata", true, "Anonymous");
            ShoppingList list = new ShoppingList("popis", new List<Ingredient>(), "Anon");

            var RecipeRepository = new Mock<IRecipeRespository>();
            var ShopRepository = new Mock<IShoppingListRepository>();

            RecipeRepository.Setup(x => x.getRecipeById(recipe.Id)).Returns(recipe);
            ShopRepository.Setup(x => x.getShoppingListById(list.Id)).Returns(list);

            DataProcessor.MockAddAllIngredientsOfRecipe(RecipeRepository.Object, ShopRepository.Object, recipe.Id,list.Id );
            RecipeRepository.Verify(x => x.getRecipeById(recipe.Id), Times.Once);
            ShopRepository.Verify(x => x.getShoppingListById(list.Id), Times.Once);
            ShopRepository.Verify(x => x.editShoppingList(list), Times.Once);

        }


        [TestMethod]
        public void Moq_EditWeeklyPlan()
        {
            Ingredient ing = new Ingredient("Fanta", 2, "L");
            Recipe recipe = new Recipe("Recept", "opis", 35, 4, new List<Ingredient>() { ing }, "Salata", true, "Anonymous");
            DailyPlan day = new DailyPlan();
            day.DayOfWeek = 0;
            day.AddBreakfast(recipe);
            WeeklyPlan week = new WeeklyPlan();
            DailyPlan[] days = new DailyPlan[7];
            for (int i = 0; i < 7; i++)
            {
                days[i] = day;
                week.AddDailyPlan(day);
            }
            
            var DailyPlanRepository = new Mock<IDailyPlanRepository>();
            var WeeklyPlanRepository = new Mock<IWeeklyPlanRepository>();

            WeeklyPlanRepository.Setup(x => x.getWeeklyPlanById(week.Id)).Returns(week);

            DataProcessor.MockEditWeeklyPlan(WeeklyPlanRepository.Object, DailyPlanRepository.Object, week, days);
            DailyPlanRepository.Verify(x => x.addDailyPlan(day), Times.Exactly(7));
            DailyPlanRepository.Verify(x => x.deleteDailyPlan(day), Times.Exactly(7));
            WeeklyPlanRepository.Verify(x => x.editWeeklyPlan(week), Times.Once);
        }


    }
}
