﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MealPlanner2.Model;
using MealPlanner2.Model.Repositories;

namespace NMock.DataProcessing.Tests
{
    [TestFixture]
    public class DataProcessingTests
    {
        [Test]
        public void NMock_GetUserByUserName()
        {
            string username = "Anon";
            User person = new User("Anon", "pass");

            MockFactory _factory = new MockFactory();
            var mockUserRepo = _factory.CreateMock<IUserRepository>();

            //expectations
            mockUserRepo.Expects.One.MethodWith(_ => _.getUserByUsername(username)).WillReturn(person);
            mockUserRepo.MockObject.getUserByUsername("Anon");
            _factory.VerifyAllExpectationsHaveBeenMet();
        }

        [Test]
        public void NMock_GetVisibleRecipes()
        {
            string username = "Anon";
            User person = new User("Anon", "pass");
            List<Ingredient> ingredients = new List<Ingredient>();
            ingredients.Add(new Ingredient("Fanta", 1, "L"));
            Recipe recipe = new Recipe("Recept", "opis", 35, 4, ingredients, "Salata", true, "Anon");
            List<Recipe> recipes = new List<Recipe>();
            recipes.Add(recipe);

            MockFactory _factory = new MockFactory();
            var mockUserRepo = _factory.CreateMock<IUserRepository>();
            var mockRecipeRepo = _factory.CreateMock<IRecipeRespository>();

            //expectations
            mockUserRepo.Expects.One.MethodWith(_ => _.getUserByUsername(username)).WillReturn(person);
            mockRecipeRepo.Expects.One.MethodWith(_ => _.getVisibleRecipes(person)).WillReturn(recipes);
            mockUserRepo.MockObject.getUserByUsername("Anon");
            mockRecipeRepo.MockObject.getVisibleRecipes(person);
            _factory.VerifyAllExpectationsHaveBeenMet();
        }

        [Test]
        public void NMock_GetShoppingLists()
        {
            string username = "Anon";
            User person = new User("Anon", "pass");
            List<Ingredient> ingredients = new List<Ingredient>();
            ingredients.Add(new Ingredient("Fanta", 1, "L"));
            ShoppingList list = new ShoppingList("popis1", ingredients, "Anon");
            ShoppingList list2 = new ShoppingList("popis2", new List<Ingredient>(), "Anon");
            List<ShoppingList> shoppingLists = new List<ShoppingList>();
            shoppingLists.AddRange(new List<ShoppingList> { list, list2 });


            MockFactory _factory = new MockFactory();
            var mockUserRepo = _factory.CreateMock<IUserRepository>();
            var mockShopListRepo = _factory.CreateMock<IShoppingListRepository>();


            //expectations
            //mockUserRepo.Expects.One.MethodWith(_ => _.getUserByUsername(username)).WillReturn(person);
            //mockShopListRepo.Expects.One.MethodWith(_ => _.getMyShoppingLists(person)).WillReturn(shoppingLists);
            //mockUserRepo.MockObject.getUserByUsername("Anon");
            //mockShopListRepo.MockObject.getMyShoppingLists(person);
            //_factory.VerifyAllExpectationsHaveBeenMet();
        }


    }
}
