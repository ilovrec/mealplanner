﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MealPlanner2.Controllers;
using MealPlanner2.Model;

namespace MealPlanner2.Presentation
{
    public partial class FormViewIngredients : Form
    {
        private MainController _mainController = null;
        private List<Ingredient> _listIngredient = null;
        public FormViewIngredients()
        {
            InitializeComponent();
        }

        public void ShowModaless(MainController mainController, List<Ingredient> ingredients)
        {
            _mainController = mainController;
            _listIngredient = ingredients;

            UpdateList();

            this.Show();
        }

        private void UpdateList()
        {
            for (int i = 0; i < _listIngredient.Count; i++)
            {
                Ingredient ingredient = _listIngredient[i];

                ListViewItem item = new ListViewItem(ingredient.Name);
                item.SubItems.Add(ingredient.Quantity.ToString());

                listIngredients.Items.Add(item);
            }
        }
    }
}
