﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MealPlanner2.BaseLib;

namespace MealPlanner2.Presentation
{
    public partial class FormLoginUser : Form, ILoginUserView
    {
        public FormLoginUser()
        {
            InitializeComponent();
        }

        public string Username => textBox1.Text;

        public string Password => textBox2.Text;

        public bool ShowViewModal()
        {
            if (this.ShowDialog() == DialogResult.OK)
                return true;
            else
                return false;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
