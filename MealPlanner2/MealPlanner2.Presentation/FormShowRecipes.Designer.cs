﻿namespace MealPlanner2.Presentation
{
    partial class FormShowRecipes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listRecipes = new System.Windows.Forms.ListView();
            this.Naziv = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Kategorija = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Preparation = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Porcije = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Id = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // listRecipes
            // 
            this.listRecipes.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Naziv,
            this.Kategorija,
            this.Preparation,
            this.Porcije,
            this.Id});
            this.listRecipes.FullRowSelect = true;
            this.listRecipes.GridLines = true;
            this.listRecipes.HideSelection = false;
            this.listRecipes.Location = new System.Drawing.Point(46, 96);
            this.listRecipes.Name = "listRecipes";
            this.listRecipes.Size = new System.Drawing.Size(469, 264);
            this.listRecipes.TabIndex = 0;
            this.listRecipes.UseCompatibleStateImageBehavior = false;
            this.listRecipes.View = System.Windows.Forms.View.Details;
            this.listRecipes.SelectedIndexChanged += new System.EventHandler(this.listRecipes_SelectedIndexChanged);
            this.listRecipes.DoubleClick += new System.EventHandler(this.listRecipes_DoubleClick);
            // 
            // Naziv
            // 
            this.Naziv.Text = "Naziv";
            this.Naziv.Width = 177;
            // 
            // Kategorija
            // 
            this.Kategorija.Text = "Kategorija";
            this.Kategorija.Width = 113;
            // 
            // Preparation
            // 
            this.Preparation.Text = "Priprema";
            this.Preparation.Width = 69;
            // 
            // Porcije
            // 
            this.Porcije.Text = "Porcije";
            // 
            // Id
            // 
            this.Id.Text = "Id";
            this.Id.Width = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(42, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(182, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Pregled svih recepata";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(43, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(227, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Dvostruki klik na recept otvara njegove detalje";
            // 
            // FormShowRecipes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(561, 369);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.listRecipes);
            this.Name = "FormShowRecipes";
            this.Text = "FormShowRecipes";
            this.Load += new System.EventHandler(this.FormShowRecipes_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView listRecipes;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.ColumnHeader Naziv;
        private System.Windows.Forms.ColumnHeader Kategorija;
        private System.Windows.Forms.ColumnHeader Preparation;
        private System.Windows.Forms.ColumnHeader Porcije;
        private System.Windows.Forms.ColumnHeader Id;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}