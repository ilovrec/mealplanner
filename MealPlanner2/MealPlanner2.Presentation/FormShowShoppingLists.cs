﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MealPlanner2.BaseLib;
using MealPlanner2.Model;
using MealPlanner2.MemoryBasedDAL;

namespace MealPlanner2.Presentation
{
    public partial class FormShowShoppingLists : Form, IShowShoppingLists
    {
        private IMainController _mainController = null;
        private List<ShoppingList> _listShoppingList = null;
        private ShoppingList currentlySelected = null;
        public FormShowShoppingLists()
        {
            InitializeComponent();
        }

        public void ShowViewModal(IMainController mainController, List<ShoppingList> shoppingLists)
        {
            _mainController = mainController;
            _listShoppingList = shoppingLists;

            UpdateList();

            this.Show();
        }

        private void UpdateList()
        {
            listShoppingLists.Items.Clear();
            for (int i = 0; i < _listShoppingList.Count; i++)
            {
                ShoppingList list = _listShoppingList[i];

                ListViewItem item = new ListViewItem(list.Name);
                item.SubItems.Add(list.ModifiedAt.ToString());
                item.SubItems.Add(list.Id.ToString());

                listShoppingLists.Items.Add(item);
            }
        }

        private void Reinitialize()
        {
            textBox2.Text = "Nije odabran popis";
            listIngredients.Clear();
            currentlySelected =null;
            label6.Text = "Naziv: Nije odabran popis";
            textBox1.Text = "";
        }

      

        private void listShoppingLists_DoubleClick(object sender, EventArgs e)
        {
            if (listShoppingLists.SelectedItems[0] != null)
            {
                int id = 0;
                Int32.TryParse(listShoppingLists.SelectedItems[0].SubItems[2].Text, out id);
                ShoppingList selectedList = _listShoppingList.Find(i => i.Id == id);
                currentlySelected = selectedList;
                updateDetailedInfo(selectedList);
            }
        }

        

        private void updateDetailedInfo(ShoppingList list)
        {
            textBox2.Text = list.Name;
            listIngredients.Items.Clear();
            List<Ingredient> listIng = list.Ingredients.ToList();

            foreach (var ing in listIng)
            {
                ListViewItem item = new ListViewItem(ing.Name);
                item.SubItems.Add(ing.Quantity.ToString()+" "+ ing.MeasureUnit);
                item.SubItems.Add(ing.Id.ToString());
                listIngredients.Items.Add(item);
            }

            label6.Text = "Naziv: " + list.Name;
            textBox1.Text = list.getIngredientsMultipleLines();
                
            

        }

        

        //dodavanje sastojaka u listu
        private void button1_Click(object sender, EventArgs e)
        {
            if (comboBox1.Text != "Odaberi mjeru" && currentlySelected != null)
            {
                ShoppingList selectedList = currentlySelected;
                Ingredient newIngredient = new Ingredient(textBox3.Text, numericUpDown2.Value, comboBox1.Text);
                selectedList.Ingredients.Add(newIngredient);
                _listShoppingList.RemoveAll(chunk => chunk.Id == selectedList.Id);
                _listShoppingList.Add(selectedList);
                textBox3.Text = "";
                numericUpDown2.Value = 1;
                comboBox1.Text = "Odaberi mjeru";
                _mainController.EditShoppingList(currentlySelected);
                updateDetailedInfo(selectedList);
                UpdateList();
            }
        }

       
        //brisanje odabrane liste
        private void button3_Click(object sender, EventArgs e)
        {
            if (textBox2.Text != "Nije odabran popis" && currentlySelected!=null)
            {
                ShoppingList selectedList = currentlySelected;
                _listShoppingList.RemoveAll(chunk => chunk.Id == selectedList.Id);
                _mainController.DeleteShoppingList(selectedList.Id);
                updateDetailedInfo(selectedList);
                UpdateList();
                Reinitialize();
            }
        }

        //brisanje odabranog sastojka
        private void button4_Click(object sender, EventArgs e)
        {
            if (listIngredients.SelectedItems.Count > 0 && currentlySelected !=null)
            {
                int id;
                Int32.TryParse(listIngredients.SelectedItems[0].SubItems[2].Text, out id);
                Ingredient selectedIng = currentlySelected.Ingredients.ToList().Find(i => i.Id == id);
                currentlySelected.Ingredients.Remove(selectedIng);
                _mainController.EditShoppingList(currentlySelected);
                updateDetailedInfo(currentlySelected);
            }
        }

        //mjenjanje naziva liste
        private void button5_Click(object sender, EventArgs e)
        {
            if (currentlySelected != null)
            {
                ShoppingList selectedList = currentlySelected;
                selectedList.Name = textBox2.Text;
                _listShoppingList.RemoveAll(chunk => chunk.Id == selectedList.Id);
                _listShoppingList.Add(selectedList);
                _mainController.EditShoppingList(selectedList);
                updateDetailedInfo(selectedList);
                UpdateList();
            }
        }

        private void listShoppingLists_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
