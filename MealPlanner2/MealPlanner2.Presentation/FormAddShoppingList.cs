﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MealPlanner2.Model;
using MealPlanner2.BaseLib;
using MealPlanner2.MemoryBasedDAL;

namespace MealPlanner2.Presentation
{
    public partial class FormAddShoppingList : Form, IAddNewShoppingListView
    {
        List<Ingredient> ingredients;
        public FormAddShoppingList()
        {
            InitializeComponent();
            ingredients = new List<Ingredient>();
        }

        public string ListName => textBox2.Text;
        public List<Ingredient> ListIngredients => ingredients;

        public bool ShowViewModal()
        {
            if (this.ShowDialog() == DialogResult.OK)
                return true;
            else
                return false;
        }



        private void button1_Click(object sender, EventArgs e)
        {
            if (comboBox1.Text != "Odaberi mjeru")
            {
                Ingredient newIngredient = new Ingredient(textBox1.Text, numericUpDown1.Value, comboBox1.Text);
                ingredients.Add(newIngredient);
                textBox1.Text = "";
                numericUpDown1.Value = 1;
                comboBox1.Text = "Odaberi mjeru";
            }
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }
    }
}
