﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MealPlanner2.Controllers;

namespace MealPlanner2.Presentation
{
    public partial class FormMain : Form
    {
        private readonly MainController _controller;
        private readonly WindowsFormsFactory _formsFactory = new WindowsFormsFactory();

        public FormMain(MainController inController)
        {
            _controller = inController;
            
            InitializeComponent();
            label2.Text = "Ulogirani korisnik:" + inController.getLoggedUser().Username;
        }

        

       

        private void addRecipe_Click(object sender, EventArgs e)
        {
            _controller.AddRecipe();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            _controller.AddShoppingList();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            _controller.ShowRecipies();
        }

        private void pregledajPopisToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _controller.ShowShoppingList();
        }

        private void dodajPopisToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _controller.AddShoppingList();
        }

        private void pregledajRecepteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _controller.ShowRecipies();
        }

        private void dodajReceptToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _controller.AddRecipe();
        }

        private void dodajToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _controller.ShowAddWeeklyPlan();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            _controller.LoginUser();
            label2.Text = "Ulogirani korisnik: " + _controller.getLoggedUser().Username;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            _controller.logoutUser();
            label2.Text = "Ulogirani korisnik: " + _controller.getLoggedUser().Username;
        }

        private void pregledajPlanoveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _controller.ShowWeeklyPlan();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            _controller.addRecipeNhibernate();
        }
    }
}
