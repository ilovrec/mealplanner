﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MealPlanner2.BaseLib;
using MealPlanner2.Model;

namespace MealPlanner2.Presentation
{
    public partial class FormShowRecipes : Form, IShowAllRecipesView
    {
        private IMainController _mainController = null;
        private List<Recipe> _listRecipes = null;
        public FormShowRecipes()
        {
            InitializeComponent();
        }

        public void ShowViewModal(IMainController mainController, List<Recipe> recipes)
        {
            _mainController = mainController;
            _listRecipes = recipes;

            UpdateList();

            this.Show();
        }

        private void UpdateList()
        {
           
            for (int i = 0; i < _listRecipes.Count; i++)
            {
                Recipe recipe = _listRecipes[i];

                ListViewItem item = new ListViewItem(recipe.Name);
                item.SubItems.Add(recipe.Category);
                item.SubItems.Add(recipe.Preparation.ToString());
                item.SubItems.Add(recipe.Portions.ToString());
                item.SubItems.Add(recipe.Id.ToString());

                listRecipes.Items.Add(item);
            }
        }

        private void listRecipes_SelectedIndexChanged(object sender, EventArgs e)
        {
            //bla
        }
        private void listRecipes_DoubleClick(object sender, EventArgs e)
        {

            if (listRecipes.SelectedItems[0] != null)
            {
                int id = 0;
                Int32.TryParse(listRecipes.SelectedItems[0].SubItems[4].Text, out id);

                _mainController.viewDetailsRecipe(id);
            }
        }

        private void FormShowRecipes_Load(object sender, EventArgs e)
        {

        }
    }
}
