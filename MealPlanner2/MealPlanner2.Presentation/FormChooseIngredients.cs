﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MealPlanner2.Model;
using MealPlanner2.BaseLib;

namespace MealPlanner2.Presentation
{
    
    public partial class FormChooseIngredients : Form, IChooseIngredientsView
    {
        private IMainController _mainController = null;
        private List<Ingredient> _listIngredients = null;
        private Ingredient selectedIngredient = null;
        private List<Ingredient> addedIngredients = null;
        private ShoppingList shopList = null;
        public FormChooseIngredients()
        {
            InitializeComponent();
        }

        public void ShowViewModal(IMainController mainController, Recipe recipe, ShoppingList shoppingList)
        {
            _mainController = mainController;
            _listIngredients = recipe.Ingredients.ToList();
            addedIngredients = new List<Ingredient>();
            shopList = shoppingList;
            label2.Text = "Dodavanje na popis:" + shopList.Name;
            label3.Text = "Sastojci recepta: " + recipe.Name;
            UpdateList();

            this.Show();
        }

        private void UpdateList()
        {
            listIngredients.Items.Clear();
            for (int i = 0; i < _listIngredients.Count; i++)
            {
                Ingredient ing = _listIngredients[i];

                ListViewItem item = new ListViewItem(ing.Name);
                item.SubItems.Add(ing.Quantity.ToString()+" " + ing.MeasureUnit.ToString());
                item.SubItems.Add(ing.Id.ToString());
                listIngredients.Items.Add(item);
            }
        }

        private void listIngredients_DoubleClick(object sender, EventArgs e)
        {
            if (listIngredients.SelectedItems[0] != null)
            {
                int id = 0;
                Int32.TryParse(listIngredients.SelectedItems[0].SubItems[2].Text, out id);
                selectedIngredient = _listIngredients.Find(i => i.Id == id);
                
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (listIngredients.SelectedItems[0] != null)
            {
                int id = 0;
                Int32.TryParse(listIngredients.SelectedItems[0].SubItems[2].Text, out id);
                selectedIngredient = _listIngredients.Find(i => i.Id == id);
                addedIngredients.Add(selectedIngredient);
                MessageBox.Show("Dodan sastojak: " + selectedIngredient.Name);

            }
          
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if(addedIngredients.Count>0)
             _mainController.AddChosenIngredientsToList(addedIngredients, shopList.Id);
            MessageBox.Show("Na listu '" + shopList.Name+"' dodani sastojci: " +Ingredient.getIngredientsNames(addedIngredients));
            this.Close();
        }
    }
}
