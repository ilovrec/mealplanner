﻿namespace MealPlanner2.Presentation
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.popisZaKupnjuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pregledajPopisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dodajPopisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.receptiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pregledajRecepteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dodajReceptToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tjedniPlanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dodajToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pregledajPlanoveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.popisZaKupnjuToolStripMenuItem,
            this.receptiToolStripMenuItem,
            this.tjedniPlanToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(514, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // popisZaKupnjuToolStripMenuItem
            // 
            this.popisZaKupnjuToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pregledajPopisToolStripMenuItem,
            this.dodajPopisToolStripMenuItem});
            this.popisZaKupnjuToolStripMenuItem.Name = "popisZaKupnjuToolStripMenuItem";
            this.popisZaKupnjuToolStripMenuItem.Size = new System.Drawing.Size(102, 20);
            this.popisZaKupnjuToolStripMenuItem.Text = "Popis za kupnju";
            // 
            // pregledajPopisToolStripMenuItem
            // 
            this.pregledajPopisToolStripMenuItem.Name = "pregledajPopisToolStripMenuItem";
            this.pregledajPopisToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.pregledajPopisToolStripMenuItem.Text = "Pregledaj popise";
            this.pregledajPopisToolStripMenuItem.Click += new System.EventHandler(this.pregledajPopisToolStripMenuItem_Click);
            // 
            // dodajPopisToolStripMenuItem
            // 
            this.dodajPopisToolStripMenuItem.Name = "dodajPopisToolStripMenuItem";
            this.dodajPopisToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.dodajPopisToolStripMenuItem.Text = "Dodaj popis";
            this.dodajPopisToolStripMenuItem.Click += new System.EventHandler(this.dodajPopisToolStripMenuItem_Click);
            // 
            // receptiToolStripMenuItem
            // 
            this.receptiToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pregledajRecepteToolStripMenuItem,
            this.dodajReceptToolStripMenuItem});
            this.receptiToolStripMenuItem.Name = "receptiToolStripMenuItem";
            this.receptiToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.receptiToolStripMenuItem.Text = "Recepti";
            // 
            // pregledajRecepteToolStripMenuItem
            // 
            this.pregledajRecepteToolStripMenuItem.Name = "pregledajRecepteToolStripMenuItem";
            this.pregledajRecepteToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.pregledajRecepteToolStripMenuItem.Text = "Pregledaj recepte";
            this.pregledajRecepteToolStripMenuItem.Click += new System.EventHandler(this.pregledajRecepteToolStripMenuItem_Click);
            // 
            // dodajReceptToolStripMenuItem
            // 
            this.dodajReceptToolStripMenuItem.Name = "dodajReceptToolStripMenuItem";
            this.dodajReceptToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.dodajReceptToolStripMenuItem.Text = "Dodaj recept";
            this.dodajReceptToolStripMenuItem.Click += new System.EventHandler(this.dodajReceptToolStripMenuItem_Click);
            // 
            // tjedniPlanToolStripMenuItem
            // 
            this.tjedniPlanToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dodajToolStripMenuItem,
            this.pregledajPlanoveToolStripMenuItem});
            this.tjedniPlanToolStripMenuItem.Name = "tjedniPlanToolStripMenuItem";
            this.tjedniPlanToolStripMenuItem.Size = new System.Drawing.Size(77, 20);
            this.tjedniPlanToolStripMenuItem.Text = "Tjedni plan";
            // 
            // dodajToolStripMenuItem
            // 
            this.dodajToolStripMenuItem.Name = "dodajToolStripMenuItem";
            this.dodajToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.dodajToolStripMenuItem.Text = "Dodaj plan";
            this.dodajToolStripMenuItem.Click += new System.EventHandler(this.dodajToolStripMenuItem_Click);
            // 
            // pregledajPlanoveToolStripMenuItem
            // 
            this.pregledajPlanoveToolStripMenuItem.Name = "pregledajPlanoveToolStripMenuItem";
            this.pregledajPlanoveToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.pregledajPlanoveToolStripMenuItem.Text = "Pregledaj planove";
            this.pregledajPlanoveToolStripMenuItem.Click += new System.EventHandler(this.pregledajPlanoveToolStripMenuItem_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.button5);
            this.groupBox1.Controls.Add(this.button4);
            this.groupBox1.Location = new System.Drawing.Point(24, 51);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(383, 166);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Korisničke akcije";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(601, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 5;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(36, 102);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 6;
            this.button4.Text = "Ulogiraj se";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(33, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "label2";
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(148, 102);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 8;
            this.button5.Text = "Odlogiraj se";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(514, 264);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FormMain";
            this.Text = "MainForm";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem popisZaKupnjuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem receptiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tjedniPlanToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ToolStripMenuItem pregledajPopisToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dodajPopisToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pregledajRecepteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dodajReceptToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dodajToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pregledajPlanoveToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button5;
    }
}