﻿namespace MealPlanner2.Presentation
{
    partial class FormShowWeeklyPlans
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listWeeks = new System.Windows.Forms.ListView();
            this.range = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.idWeek = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.listSelectedWeek = new System.Windows.Forms.ListView();
            this.Dan = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Doručak = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Ručak = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Večera = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.weekRange = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // listWeeks
            // 
            this.listWeeks.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.range,
            this.idWeek});
            this.listWeeks.HideSelection = false;
            this.listWeeks.Location = new System.Drawing.Point(27, 77);
            this.listWeeks.Name = "listWeeks";
            this.listWeeks.Size = new System.Drawing.Size(149, 282);
            this.listWeeks.TabIndex = 0;
            this.listWeeks.UseCompatibleStateImageBehavior = false;
            this.listWeeks.View = System.Windows.Forms.View.Details;
            this.listWeeks.DoubleClick += new System.EventHandler(this.listWeeks_DoubleClick);
            // 
            // range
            // 
            this.range.Text = "Tjedan";
            this.range.Width = 144;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(45, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(196, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Pregled tjednih planova";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.listSelectedWeek);
            this.groupBox1.Controls.Add(this.weekRange);
            this.groupBox1.Location = new System.Drawing.Point(242, 77);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(672, 282);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Detalji o tjednom planu";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(153, 238);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(119, 23);
            this.button2.TabIndex = 3;
            this.button2.Text = "Izbriši tjedni plan";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(16, 238);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(117, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Uredi tjedni plan";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // listSelectedWeek
            // 
            this.listSelectedWeek.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Dan,
            this.Doručak,
            this.Ručak,
            this.Večera});
            this.listSelectedWeek.GridLines = true;
            this.listSelectedWeek.HideSelection = false;
            this.listSelectedWeek.Location = new System.Drawing.Point(16, 53);
            this.listSelectedWeek.Name = "listSelectedWeek";
            this.listSelectedWeek.Size = new System.Drawing.Size(641, 171);
            this.listSelectedWeek.TabIndex = 1;
            this.listSelectedWeek.UseCompatibleStateImageBehavior = false;
            this.listSelectedWeek.View = System.Windows.Forms.View.Details;
            // 
            // Dan
            // 
            this.Dan.Text = "Dan";
            this.Dan.Width = 90;
            // 
            // Doručak
            // 
            this.Doručak.Text = "Doručak";
            this.Doručak.Width = 176;
            // 
            // Ručak
            // 
            this.Ručak.Text = "Ručak";
            this.Ručak.Width = 177;
            // 
            // Večera
            // 
            this.Večera.Text = "Večera";
            this.Večera.Width = 191;
            // 
            // weekRange
            // 
            this.weekRange.AutoSize = true;
            this.weekRange.Location = new System.Drawing.Point(22, 33);
            this.weekRange.Name = "weekRange";
            this.weekRange.Size = new System.Drawing.Size(0, 13);
            this.weekRange.TabIndex = 0;
            // 
            // FormShowWeeklyPlans
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(966, 450);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.listWeeks);
            this.Name = "FormShowWeeklyPlans";
            this.Text = "FormShowWeeklyPlans";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView listWeeks;
        private System.Windows.Forms.ColumnHeader range;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ListView listSelectedWeek;
        private System.Windows.Forms.ColumnHeader Dan;
        private System.Windows.Forms.ColumnHeader Doručak;
        private System.Windows.Forms.ColumnHeader Ručak;
        private System.Windows.Forms.ColumnHeader Večera;
        private System.Windows.Forms.Label weekRange;
        private System.Windows.Forms.ColumnHeader idWeek;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}