﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MealPlanner2.BaseLib;
using MealPlanner2.Controllers;

namespace MealPlanner2.Presentation
{
    public partial class FormAddIngredient : Form, IAddNewIngredientView
    {
        public string IngredientName => textBox1.Text;
        public string IngredientQuantity => textBox2.Text;

        public FormAddIngredient()
        {
            InitializeComponent();
        }

        public bool ShowViewModal()
        {
            if (this.ShowDialog() == DialogResult.OK)
                return true;
            else
                return false;
        }

     

        
    }
}
