﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MealPlanner2.BaseLib;
using MealPlanner2.Model;

namespace MealPlanner2.Presentation
{
    public class WindowsFormsFactory : IWindowsFormsFactory
    {
        /* public IAddUserView CreateAddNewUserView()
         {
             var newUserRegistration = new RegisterUser();

             return newUserRegistration;
         }

         public ILoginUserView CreateLoginUserView()
         {
             var newLogin = new LoginUser();
             return newLogin;
         } */

        public IAddNewIngredientView CreateAddNewIngredientView()
        {
            var newIngredient = new FormAddIngredient();
            return newIngredient;
        }

        public IAddNewRecipeView CreateAddNewRecipeView()
        {
            return new FormAddRecipe();
        }

        public IAddNewShoppingListView CreateAddNewShoppingListView()
        {
            return new FormAddShoppingList();
        }

        public IAddWeeklyPlanView CreateAddNewWeeklyPlanView()
        {
            return new FormAddWeeklyPlan();
        }

        public IChooseIngredientsView CreateChooseIngredientsView()
        {
            return new FormChooseIngredients();
        }

        public ILoginUserView CreateLoginUserView()
        {
            return new FormLoginUser();
        }

        public IShowAllRecipesView CreateShowAllRecipesView()
        {
            return new FormShowRecipes();
        }

        public IShowShoppingLists CreateShowAllShoppingListsView()
        {
            return new FormShowShoppingLists();
        }

        public IShowWeeklyPlansView CreateShowAllWeeklyPlansView()
        {
            return new FormShowWeeklyPlans();
        }

        public IViewDetailsRecipe CreateViewDetailsRecipeView()
        {
            return new FromViewDetailsRecipe();
        }
    }
}
