﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MealPlanner2.BaseLib;
using MealPlanner2.Model;
using MealPlanner2.Model.Repositories;

namespace MealPlanner2.Presentation
{
    public partial class FormShowWeeklyPlans : Form, IShowWeeklyPlansView
    {
        private IMainController _mainController = null;
        private List<WeeklyPlan> _listWeeklyPlans = null;
        private IRecipeRespository _recipeRepo = null;
        WeeklyPlan selectedWeek = null;
        public FormShowWeeklyPlans()
        {
            InitializeComponent();
        }

        public void ShowViewModal(IMainController mainController, List<WeeklyPlan> allWeeklyPlans, IRecipeRespository recipeRepo)
        {
            _mainController = mainController;
            _listWeeklyPlans = allWeeklyPlans;
            _recipeRepo = recipeRepo;
            UpdateList();

            this.Show();
        }

        private void UpdateList()
        {
            listWeeks.Items.Clear();
            for (int i = 0; i < _listWeeklyPlans.Count; i++)
            {
                WeeklyPlan plan = _listWeeklyPlans[i];
                DateTime monday = plan.Monday;
                DateTime sunday = monday.AddDays(7); ;

                ListViewItem item = new ListViewItem(monday.ToShortDateString() +" - " + sunday.ToShortDateString());
                item.SubItems.Add(plan.Id.ToString());
                listWeeks.Items.Add(item);
            }
        }

        private void UpdateDetailsList()
        {
            listSelectedWeek.Items.Clear();
            for (int i = 0; i < selectedWeek.PlansArray.Count; i++)
            {

                DailyPlan day = selectedWeek.PlansArray[i];

                ListViewItem item = new ListViewItem(day.getDayName());
                item.SubItems.Add(Recipe.getRecipeListAsString(_recipeRepo.getRecipeByIds(day.Breakfast.ToList())));
                item.SubItems.Add(Recipe.getRecipeListAsString(_recipeRepo.getRecipeByIds(day.Lunch.ToList())));
                item.SubItems.Add(Recipe.getRecipeListAsString(_recipeRepo.getRecipeByIds(day.Dinner.ToList())));
                listSelectedWeek.Items.Add(item);
            }
        } 

        private void listWeeks_DoubleClick(object sender, EventArgs e)
        {
            if (listWeeks.SelectedItems[0] != null)
            {
                int id = 0;
                Int32.TryParse(listWeeks.SelectedItems[0].SubItems[1].Text, out id);
                selectedWeek =_listWeeklyPlans.Find(i => i.Id == id);
                UpdateDetailsList();
                weekRange.Text = listWeeks.SelectedItems[0].Text;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (listWeeks.SelectedItems.Count > 0 && selectedWeek!=null)
            {
                _mainController.EditWeeklyPlan(selectedWeek);
                this.Close();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (listWeeks.SelectedItems.Count > 0 && selectedWeek!=null)
            {
                _mainController.DeleteWeeklyPlan(selectedWeek.Id);
                _listWeeklyPlans.RemoveAll(chunk => chunk.Id == selectedWeek.Id);
                UpdateList();
                listSelectedWeek.Items.Clear();
            }
        }
    }
}
