﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MealPlanner2.BaseLib;
using MealPlanner2.Model;
using MealPlanner2.MemoryBasedDAL;
using MealPlanner2.Model.Repositories;

namespace MealPlanner2.Presentation
{
    public partial class FormAddWeeklyPlan : Form, IAddWeeklyPlanView
    {
        private IMainController _mainController = null;
        private IRecipeRespository _recipeRepo = null;
        private List<Recipe> _listRecipes = null;
        private DailyPlan selectedDay = null;
        private DailyPlan[] _allDays = null;
        private int idWeeklyPlan = -100;
        public FormAddWeeklyPlan()
        {
            InitializeComponent();
        }

        public void ShowViewModal(IMainController mainController, List<Recipe> recipes, WeeklyPlan plan, IRecipeRespository recipeRepo)
        {
            _mainController = mainController;
            _listRecipes = recipes;
            _recipeRepo = recipeRepo;

            UpdateList();
            initializeData(plan);
            this.Show();
        }

        public void initializeData(WeeklyPlan plan)
        {
            if (plan != null)
            {
                _allDays = plan.PlansArray.ToArray();
                dateTimePicker1.Value = plan.Monday;
                selectedDay = _allDays[0];
                updateMealLists(selectedDay);
                idWeeklyPlan = plan.Id;
            }
            else
            {
                _allDays = new DailyPlan[7];
                for (int i = 0; i < _allDays.Length; i++)
                {
                    int id = WeeklyPlanRepository.getInstance().getNewIDaily();
                    _allDays[i] = new DailyPlan(i);
                }
                selectedDay = _allDays[0];
            }

        }

        private void UpdateList()
        {

            for (int i = 0; i < _listRecipes.Count; i++)
            {
                Recipe recipe = _listRecipes[i];

                ListViewItem item = new ListViewItem(recipe.Name);
                item.SubItems.Add(recipe.Category);
                item.SubItems.Add(recipe.Id.ToString());

                listRecipes.Items.Add(item);
            }
        }
      

       

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string day = comboBox1.Text;
            int index = getIndexFromDay(day);
            if (_allDays[index] == null)
            {
                _allDays[index] = new DailyPlan(index);
            }
            selectedDay = _allDays[index];
            updateMealLists(selectedDay);
        }

       
        private void updateMealLists(DailyPlan plan)
        {
            updateBreakfastList(_recipeRepo.getRecipeByIds(plan.Breakfast.ToList()));
            updateLunchList(_recipeRepo.getRecipeByIds(plan.Lunch.ToList()));
            updateDinnerList(_recipeRepo.getRecipeByIds(plan.Dinner.ToList()));
        }

        public void updateBreakfastList(List<Recipe> recipes)
        {
            listBreakfast.Items.Clear();
            for (int i = 0; i < recipes.Count; i++)
            {
                Recipe recipe = recipes[i];
                ListViewItem item = new ListViewItem(recipe.Name);
                item.SubItems.Add(recipe.Id.ToString());

                listBreakfast.Items.Add(item);
            }
        }

        public void updateLunchList(List<Recipe> recipes)
        {
            listLunch.Items.Clear();
            for (int i = 0; i < recipes.Count; i++)
            {
                Recipe recipe = recipes[i];
                ListViewItem item = new ListViewItem(recipe.Name);
                item.SubItems.Add(recipe.Id.ToString());
                listLunch.Items.Add(item);
            }
        }

        public void updateDinnerList(List<Recipe> recipes)
        {
            listDinner.Items.Clear();
            for (int i = 0; i < recipes.Count; i++)
            {
                Recipe recipe = recipes[i];
                ListViewItem item = new ListViewItem(recipe.Name);
                item.SubItems.Add(recipe.Id.ToString());
                listDinner.Items.Add(item);
            }
        }

        //dodavanje recepta u dorucak
        private void Button1_Click(object sender, EventArgs e)
        {
            int id = 0;
            Int32.TryParse(listRecipes.SelectedItems[0].SubItems[2].Text, out id);
            Recipe selectedRecipe = _listRecipes.Find(i => i.Id == id);
            selectedDay.AddBreakfast(selectedRecipe);
            updateBreakfastList(_recipeRepo.getRecipeByIds(selectedDay.Breakfast.ToList()));
        }

        private int getIndexFromDay(string day)
        {
            switch (day)
            {
                case "Ponedjeljak": return 0;
                case "Utorak":
                    return 1;
                case "Srijeda":
                    return 2;
                case "Četvrtak":
                    return 3;
                case "Petak":
                    return 4;
                case "Subota":
                    return 5;
                case "Nedjelja":
                    return 6;
            }
            return 7;

        }

        //dodavanje recepta u rucak
        private void button5_Click(object sender, EventArgs e)
        {
            int id = 0;
            Int32.TryParse(listRecipes.SelectedItems[0].SubItems[2].Text, out id);
            Recipe selectedRecipe = _listRecipes.Find(i => i.Id == id);
            selectedDay.AddLunch(selectedRecipe);
            updateLunchList(_recipeRepo.getRecipeByIds(selectedDay.Lunch.ToList()));
        }

        //dodavanje recepta u veceru
        private void button7_Click(object sender, EventArgs e)
        {
            int id = 0;
            Int32.TryParse(listRecipes.SelectedItems[0].SubItems[2].Text, out id);
            Recipe selectedRecipe = _listRecipes.Find(i => i.Id == id);
            selectedDay.AddDinner(selectedRecipe);
            updateDinnerList(_recipeRepo.getRecipeByIds(selectedDay.Dinner.ToList()));
        }

        //brisanje recepta iz doručka
        private void button3_Click(object sender, EventArgs e)
        {
            if (listBreakfast.SelectedItems.Count == 0) return;
            if (listBreakfast.SelectedItems[0] != null)
            {
                int id;
                Int32.TryParse(listBreakfast.SelectedItems[0].SubItems[1].Text, out id);
                Recipe selectedRecipe = _listRecipes.Find(i => i.Id == id);
                selectedDay.Breakfast.Remove(selectedRecipe.Id);
                updateBreakfastList(_recipeRepo.getRecipeByIds(selectedDay.Breakfast.ToList()));
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (listLunch.SelectedItems.Count == 0) return;
            if (listLunch.SelectedItems[0] != null)
            {
                int id;
                Int32.TryParse(listLunch.SelectedItems[0].SubItems[1].Text, out id);
                Recipe selectedRecipe = _listRecipes.Find(i => i.Id == id);
                selectedDay.Lunch.Remove(selectedRecipe.Id);
                updateLunchList(_recipeRepo.getRecipeByIds(selectedDay.Lunch.ToList()));
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (listDinner.SelectedItems.Count == 0) return;
            if (listDinner.SelectedItems[0] != null)
            {
                int id;
                Int32.TryParse(listDinner.SelectedItems[0].SubItems[1].Text, out id);
                Recipe selectedRecipe = _listRecipes.Find(i => i.Id == id);
                selectedDay.Dinner.Remove(selectedRecipe.Id);
                updateDinnerList(_recipeRepo.getRecipeByIds(selectedDay.Dinner.ToList()));
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int id = 0;
            Int32.TryParse(listRecipes.SelectedItems[0].SubItems[2].Text, out id);
            _mainController.viewDetailsRecipe(id);
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void groupBox4_Enter(object sender, EventArgs e)
        {

        }

        private void button8_Click(object sender, EventArgs e)
        {
            DateTime date = dateTimePicker1.Value;
            _mainController.AddWeeklyPlan(_allDays, date, idWeeklyPlan);
            this.Close();
        }
    }
}
