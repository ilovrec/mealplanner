﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MealPlanner2.BaseLib;
using MealPlanner2.Model;

namespace MealPlanner2.Presentation
{
    public partial class FormAddRecipe : Form, IAddNewRecipeView
    {
        List<Ingredient> ingredients;
        public FormAddRecipe()
        {
            InitializeComponent();
            ingredients = new List<Ingredient>();
        }

        public string RecipeName => textBox4.Text;

        public string RecipeDescription => textBox3.Text;
        public string RecipeCategory => comboBox2.Text;

        public List<Ingredient> RecipeIngredients => ingredients;

        public User RecipeOwner => throw new NotImplementedException();

        public decimal RecipePreparation => numericUpDown2.Value;

        public decimal RecipePortions => numericUpDown1.Value;
        public bool IsPublic => isPublic.Checked;

        public void setInfoRecipe(Recipe recipe)
        {
            textBox4.Text = recipe.Name;
            comboBox2.Text = recipe.Category;
            ingredients = (List<Ingredient>)recipe.Ingredients;
        }


        public bool ShowViewModal()
        {
            if (this.ShowDialog() == DialogResult.OK )
                return true;
            else
                return false;
        }

      

        private void button1_Click(object sender, EventArgs e)
        {
            if (comboBox1.Text != "Odaberi mjeru")
            {
                Ingredient newIngredient = new Ingredient(textBox1.Text, numericUpDown3.Value, comboBox1.Text);
                ingredients.Add(newIngredient);
                textBox1.Text = "";
                numericUpDown3.Value = 1;
                comboBox1.Text = "Odaberi mjeru";
            }
        }

      
    }
}
