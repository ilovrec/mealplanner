﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MealPlanner2.Model;
using MealPlanner2.BaseLib;
using MealPlanner2.MemoryBasedDAL;

namespace MealPlanner2.Presentation
{
    public partial class FromViewDetailsRecipe : Form,IViewDetailsRecipe
    {
        private IMainController _mainController = null;
        private Recipe recipe = null;
        public FromViewDetailsRecipe()
        {
            InitializeComponent();
            
        }

        

        public void ShowViewModal(IMainController mainController, Recipe recipe, List<ShoppingList> shoppingLists)
        {
            List<ShoppingList> shoppingLists2 = shoppingLists;
            this.recipe = recipe;
            _mainController = mainController;
            label2.Text = "Naziv: " + recipe.Name;
            label3.Text = "Vrijeme pripreme: " + recipe.Preparation.ToString();
            label4.Text = "Broj porcija: " + recipe.Portions.ToString();
            textBox1.Text = recipe.Description;
            comboBox1.Items.AddRange(shoppingLists.Select(s => s.Id+":"+(string)s.Name).ToList().ToArray());
            comboBox1.Text="Novi popis";
            textBox2.Text = recipe.getIngredientsAsString();
            this.Show();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            _mainController.AddAllIngredientsToList(recipe.Id, comboBox1.Text);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            _mainController.ShowAddChosenIngredients(recipe.Id, comboBox1.Text);
        }
    }
}
