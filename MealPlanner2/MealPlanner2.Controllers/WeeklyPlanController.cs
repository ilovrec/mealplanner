﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using MealPlanner2.BaseLib;
using MealPlanner2.Model;
using MealPlanner2.Model.Repositories;
using System.Windows.Forms;

namespace MealPlanner2.Controllers
{
    public class WeeklyPlanController
    {
		public void ShowAddNewWeeklyPlanForm(IAddWeeklyPlanView inForm, IMainController mainController, IRecipeRespository recipeRespository)
		{
			List<Recipe> allRecipes = recipeRespository.getVisibleRecipes(mainController.getLoggedUser());
			inForm.ShowViewModal(mainController, allRecipes, null, recipeRespository);
		}

		public void AddWeeklyPlan(IWeeklyPlanRepository weeklyRepo, DateTime date, DailyPlan[] dailyPlans, IMainController mainController, int id)
		{
			DateTime monday = WeeklyPlan.getMonday(date);

			WeeklyPlan existingPlan = weeklyRepo.getWeeklyPlanById(id);
			if (existingPlan != null)
			{
				existingPlan.Monday = monday;
				existingPlan.deleteDailyPlans();
				foreach (var day in dailyPlans)
					existingPlan.AddDailyPlan(day);
				WeeklyPlan vla = existingPlan;
				weeklyRepo.editWeeklyPlan(existingPlan);
			}
			else
			{
				WeeklyPlan weekly = new WeeklyPlan(dailyPlans, monday, mainController.getLoggedUser().Username);
				try
				{
					weeklyRepo.addWeeklyPlan(weekly);
				}catch(Exception e)
				{
					MessageBox.Show(e.Message);
				}
					
			}
		}

		public void ShowWeeklyPlans(IShowWeeklyPlansView inForm, IMainController mainController, IWeeklyPlanRepository weeklyRepo, IRecipeRespository recipeRepo)
		{
			List<WeeklyPlan> userPlans = weeklyRepo.getVisibleWeeklyPlans(mainController.getLoggedUser());
			inForm.ShowViewModal(mainController, userPlans, recipeRepo);
		}

		public void EditWeeklyPlan(IAddWeeklyPlanView inForm, IMainController mainController, IRecipeRespository recipeRespository, WeeklyPlan plan)
		{
			List<Recipe> allRecipes = recipeRespository.getVisibleRecipes(mainController.getLoggedUser());
			inForm.ShowViewModal(mainController, allRecipes, plan, recipeRespository);
		}
		
		public void DeleteWeeklyPlan(int id, IWeeklyPlanRepository weeklyRepo) {
			WeeklyPlan planToDelete= weeklyRepo.getWeeklyPlanById(id);
			weeklyRepo.deleteWeeklyPlan(planToDelete);
		}
	}
}
