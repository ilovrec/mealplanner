﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MealPlanner2.BaseLib;
using MealPlanner2.Model.Repositories;
using MealPlanner2.Model;
using System.Windows.Forms;

namespace MealPlanner2.Controllers
{
    public class UserController
    {
        public void LoginUser(ILoginUserView inForm,IUserRepository userRepo, IMainController mainController)
        {
			if (inForm.ShowViewModal() == true)
			{

				string Username = inForm.Username;
				string Password = inForm.Password;
				User existing = userRepo.getUserByUsername(Username);
				if (existing != null)
				{
					if (existing.Password == Password)
						mainController.setLoggedUser(existing);
					else
						MessageBox.Show("Kriva lozinka");
				}
				else
				{
					User newUser = new User(Username, Password);
					userRepo.addUser(newUser);
					mainController.setLoggedUser(newUser);
				}



			}
		}
    }
}
