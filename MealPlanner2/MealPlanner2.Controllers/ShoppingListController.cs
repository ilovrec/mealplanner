﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MealPlanner2.BaseLib;
using MealPlanner2.Model.Repositories;
using MealPlanner2.Model;
using System.Windows.Forms;

namespace MealPlanner2.Controllers
{
    public class ShoppingListController
    {
		public void AddNewShoppingList(IAddNewShoppingListView inForm, IShoppingListRepository listRepository, IMainController mainController)
		{
			if (inForm.ShowViewModal() == true)
			{

				string Name = inForm.ListName;
				List<Ingredient> Ingredients = inForm.ListIngredients;


				ShoppingList newList = new ShoppingList(Name, Ingredients, mainController.getLoggedUser().Username);

				listRepository.addShoppingList(newList);

			}
		}

		public void AddAllIngredientsToShoppingList(IShoppingListRepository listRepository, string listName, IRecipeRespository recipeRespository, int idRecipe, IMainController mainController)
		{
			Recipe recipe = recipeRespository.getRecipeById(idRecipe);
			int idShopList;
			string idShopListStr = listName.Split(':')[0];
			Int32.TryParse(idShopListStr, out idShopList);
			ShoppingList shoppingList = listRepository.getShoppingListById(idShopList);
			if (shoppingList == null)
			{
				shoppingList = new ShoppingList(listName, recipe.Ingredients.ToList(), mainController.getLoggedUser().Username);
				listRepository.addShoppingList(shoppingList);
			}
			else
			{
				foreach (var ing in recipe.Ingredients)
				{
					if (shoppingList.Ingredients.Where(i => i.Id == ing.Id) != null)
						shoppingList.Ingredients.Add(new Ingredient(ing.Name, ing.Quantity, ing.MeasureUnit));
					else
						shoppingList.Ingredients.Add(ing);
				}
				listRepository.editShoppingList(shoppingList);
			}
			MessageBox.Show("Na popis "+shoppingList.Name+" dodani svi sastojci recepta "+recipe.Name);
		}

		public void AddChosenIngredientsToShoppingList(IShoppingListRepository listRepository, List<Ingredient> ingredients, int id)
		{
			ShoppingList list = listRepository.getShoppingListById(id);
			foreach (var ing in ingredients)
			{
				if (list.Ingredients.Where(i => i.Id == ing.Id)!=null)
					list.Ingredients.Add(new Ingredient(ing.Name, ing.Quantity, ing.MeasureUnit));
				else
					list.Ingredients.Add(ing);
			}
			listRepository.editShoppingList(list);
		}

		public void ShowAddChosenIngredients(IChooseIngredientsView inForm, IMainController mainController, IShoppingListRepository listRepository,
														IRecipeRespository recipeRespository, int idRecipe, string idNameList)
		{
			string idShopListStr = idNameList.Split(':')[0];
			int idShopList;
			Int32.TryParse(idShopListStr, out idShopList);
			ShoppingList shoppingList = listRepository.getShoppingListById(idShopList);
			Recipe recipe = recipeRespository.getRecipeById(idRecipe);

			if (shoppingList == null)
			{
				shoppingList = new ShoppingList(idNameList, new List<Ingredient>(), mainController.getLoggedUser().Username);
				listRepository.addShoppingList(shoppingList);
			}

			inForm.ShowViewModal(mainController, recipe, shoppingList);

		}

		public void ShowAllShoppingLists(IShowShoppingLists inForm, IShoppingListRepository listRepository, IMainController mainController) 
		{
			List<ShoppingList> lists = listRepository.getMyShoppingLists(mainController.getLoggedUser());
			inForm.ShowViewModal(mainController, lists);
		}

		public void EditShoppingList(IShoppingListRepository listRepository, ShoppingList shoppingList)
		{
			listRepository.editShoppingList(shoppingList);
		}

		public void DeleteShoppingList(IShoppingListRepository listRepository,int id)
		{
			ShoppingList listToDelete = listRepository.getShoppingListById(id);
			listRepository.deleteShoppingList(listToDelete);
		}



	}
}
