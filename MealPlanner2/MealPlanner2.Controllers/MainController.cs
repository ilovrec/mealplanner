﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MealPlanner2.BaseLib;
using MealPlanner2.Model.Repositories;
using MealPlanner2.Model;

namespace MealPlanner2.Controllers
{
	public class MainController: IMainController
	{
		private readonly IWindowsFormsFactory _formsFactory = null;
		private readonly IRecipeRespository _recipeRepository = null;
		private readonly IShoppingListRepository _shopListRepository = null;
		private readonly IWeeklyPlanRepository _weeklyPlanRepository = null;
		private readonly IUserRepository _userRepository = null;
		private User loggedUser = null;
		private User anon = null;

		public MainController(IWindowsFormsFactory inFormFactory, IRecipeRespository inRecipeRepo, 
			IShoppingListRepository inListRepo, IWeeklyPlanRepository inWeeklyRepo, IUserRepository inUserRepo)
		{
			_formsFactory = inFormFactory;
			_recipeRepository = inRecipeRepo;
			_shopListRepository = inListRepo;
			_weeklyPlanRepository = inWeeklyRepo;
			_userRepository = inUserRepo;
			
			anon = inUserRepo.getUserByUsername("Anonimni korisnik");
			loggedUser = anon;
		}

		public void setLoggedUser(User user)
		{
			loggedUser = user;
		}
		public void logoutUser()
		{
			loggedUser = anon;
		}

		public User getLoggedUser()
		{
			return loggedUser;
		}

		public void AddAllIngredientsToList(int idRecipe, string listName)
		{
			var shopListController = new ShoppingListController();
			shopListController.AddAllIngredientsToShoppingList( _shopListRepository, listName, _recipeRepository, idRecipe, this);
		}


		public void ShowAddChosenIngredients(int idRecipe, string idNameList)
		{
			var shopListController = new ShoppingListController();
			var newFrm = _formsFactory.CreateChooseIngredientsView();
			shopListController.ShowAddChosenIngredients(newFrm, this, _shopListRepository, _recipeRepository, idRecipe, idNameList);
		}

		

		public void AddRecipe()
		{
			var recipeController = new RecipeController();
			var newFrm = _formsFactory.CreateAddNewRecipeView();
			recipeController.AddNewRecipe(newFrm, _recipeRepository, this);
		}

		public void AddShoppingList()
		{
			var shopListController = new ShoppingListController();
			var newFrm = _formsFactory.CreateAddNewShoppingListView();
			shopListController.AddNewShoppingList(newFrm, _shopListRepository, this);
		}

		public void AddWeeklyPlan(DailyPlan[] dailyPlans, DateTime date, int id)
		{
			var weeklyPlanController = new WeeklyPlanController();
			weeklyPlanController.AddWeeklyPlan( _weeklyPlanRepository, date, dailyPlans, this, id);
		}

		public void DeleteShoppingList(int id)
		{
			var shopListController = new ShoppingListController();
			shopListController.DeleteShoppingList(_shopListRepository, id);
		}

		public void EditShoppingList(ShoppingList shoppingList)
		{
			var shopListController = new ShoppingListController();
			shopListController.EditShoppingList(_shopListRepository, shoppingList);
		}

		public void LoginUser()
		{
			var userController = new UserController();
			var newFrm = _formsFactory.CreateLoginUserView();
			userController.LoginUser(newFrm, _userRepository, this);
		}

		public void ShowAddWeeklyPlan()
		{
			var weeklyPlanController = new WeeklyPlanController();
			var newFrm = _formsFactory.CreateAddNewWeeklyPlanView();
			weeklyPlanController.ShowAddNewWeeklyPlanForm(newFrm, this, _recipeRepository);
		}

		public void ShowRecipies()
		{
			var recipeController = new RecipeController();
			var newFrm = _formsFactory.CreateShowAllRecipesView();
			recipeController.ShowRecipes(newFrm, _recipeRepository, this);
		}

		public void ShowShoppingList()
		{
			var shopListController = new ShoppingListController();
			var newFrm = _formsFactory.CreateShowAllShoppingListsView();
			shopListController.ShowAllShoppingLists(newFrm, _shopListRepository, this);
		}

		public void ShowWeeklyPlan()
		{
			var weeklyPlanController = new WeeklyPlanController();
			var newFrm = _formsFactory.CreateShowAllWeeklyPlansView();
			weeklyPlanController.ShowWeeklyPlans(newFrm, this, _weeklyPlanRepository, _recipeRepository);
		}

		public void viewDetailsRecipe(int id)
		{
			var recipeController = new RecipeController();
			var newFrm = _formsFactory.CreateViewDetailsRecipeView();
			recipeController.ViewDetailsRecipe(newFrm, _recipeRepository, this, id, _shopListRepository);
		}

		public void EditWeeklyPlan(WeeklyPlan plan)
		{
			var weeklyPlanController = new WeeklyPlanController();
			var newFrm = _formsFactory.CreateAddNewWeeklyPlanView();
			weeklyPlanController.EditWeeklyPlan(newFrm, this, _recipeRepository, plan);
		}

		public void DeleteWeeklyPlan(int id)
		{
			var weeklyPlanController = new WeeklyPlanController();
			weeklyPlanController.DeleteWeeklyPlan(id, _weeklyPlanRepository );
		}

		public void AddChosenIngredientsToList(List<Ingredient> ingredients, int id)
		{
			var shopListController = new ShoppingListController();
			shopListController.AddChosenIngredientsToShoppingList(_shopListRepository,ingredients, id );
		}

		public void addRecipeNhibernate()
		{
			//List<Ingredient> recipes = new List<Ingredient>();
			//recipes.Add(new Ingredient("Šunka", 10, "dg", _ingredientRepository.getNewID()));
			//Recipe rec = new Recipe("nhib rec", "bla vbla opis", 45, 3, recipes, "Juha", true, this.getLoggedUser());
			//NHRecipeRepository recipeRepo = NHRecipeRepository.GetInstance();
			//recipeRepo.addRecipe(rec);
		}
	}	
}
