﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MealPlanner2.BaseLib;
using MealPlanner2.Model;
using MealPlanner2.Model.Repositories;

namespace MealPlanner2.Controllers
{
    public class RecipeController
    {
		public void AddNewRecipe(IAddNewRecipeView inForm, IRecipeRespository recipeRepository, IMainController mainController)
		{
			if (inForm.ShowViewModal() == true)
			{
				
				string Name = inForm.RecipeName;
				if (Name == "")
				{
					MessageBox.Show("Ne može se spremiti recept bez imena");
				}
					string Description = inForm.RecipeDescription;
				List<Ingredient> Ingredients2 = inForm.RecipeIngredients;
				decimal Portions = inForm.RecipePortions;
				decimal Preparation = inForm.RecipePreparation;
				string Category = inForm.RecipeCategory;
				bool isPublic = inForm.IsPublic;
				User owner = mainController.getLoggedUser();

				try
				{
					Recipe newRecipe = new Recipe(Name, Description, Preparation, Portions, Ingredients2, Category, isPublic, owner.Username);
					recipeRepository.addRecipe(newRecipe);
				}
				catch(Exception e)
				{
					MessageBox.Show("Neispravna vrijednost kategorije recepta");
				}
				
				

			}
		}

		public void ShowRecipes(IShowAllRecipesView inForm, IRecipeRespository recipeRepository, IMainController mainController)
		{
			List<Recipe> allRecipes = recipeRepository.getVisibleRecipes(mainController.getLoggedUser());
			inForm.ShowViewModal(mainController, allRecipes);
		}

		public void ViewDetailsRecipe(IViewDetailsRecipe inForm, IRecipeRespository recipeRepository, IMainController mainController, int id, IShoppingListRepository listRepository)
		{
			//List<ShoppingList> lists = listRepository.getMyShoppingLists(mainController.getLoggedUser());

			Recipe recipe = recipeRepository.getRecipeById(id);
			List<ShoppingList> existingLists=listRepository.getMyShoppingLists(mainController.getLoggedUser()).ToList();
			inForm.ShowViewModal(mainController, recipe, existingLists);
		}
	}
}
