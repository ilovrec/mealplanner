﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using MealPlanner2.Model;

namespace MealPlanner2.DALHibernate2.Mappings
{
    public class IngredientMap: ClassMap<Ingredient>
    {
        public IngredientMap()
        {
            Id(x => x.Id);
            Map(x => x.Name);
            Map(x => x.Quantity);
            Map(x => x.MeasureUnit);
            References(x => x.Recipe);
        }
    }
}
