﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MealPlanner2.Model;
using MealPlanner2.Model.Repositories;
using NHibernate;

namespace MealPlanner2.DALHibernate2.Repositories
{
    public class NHRecipeRepository : IRecipeRespository
    {
        private static NHRecipeRepository _nhRecipeRepository;
        private IList<Recipe> _listRecipes = new List<Recipe>();

        public static NHRecipeRepository GetInstance()
        {
            if (_nhRecipeRepository == null)
            {
                _nhRecipeRepository = new NHRecipeRepository();
                //_nhRecipeRepository.FillRepo();
            }

            return _nhRecipeRepository;
        }
        private void LoadRecipesFromDatabase()
        {
            using (ISession session = NHibernateHelper.NhibernateService.OpenSession())
            {
                IQuery query = session.CreateQuery(
                    "from Recipe as ex order by ex.RecipeName asc");
                _listRecipes = query.List<Recipe>();
            }
        }



        public void addRecipe(Recipe recipe)
        {
            LoadRecipesFromDatabase();

            using (ISession session = NHibernateHelper.NhibernateService.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    session.Save(recipe);
                    transaction.Commit();
                }
            }

            LoadRecipesFromDatabase();
        }

        public void deleteRecipe(Recipe recipe)
        {
            throw new NotImplementedException();
        }

        public void editRecipe(Recipe recipe)
        {
            throw new NotImplementedException();
        }

        public int getNewID()
        {
            throw new NotImplementedException();
        }

        public Recipe getRecipeById(int id)
        {
            throw new NotImplementedException();
        }

        public Recipe getRecipeByName(string name)
        {
            throw new NotImplementedException();
        }

        public List<Recipe> getRecipes()
        {
            throw new NotImplementedException();
        }

        public List<Recipe> getVisibleRecipes(User user)
        {
            throw new NotImplementedException();
        }

        public int GetNewId()
        {
            throw new NotImplementedException();
        }
    }
}
