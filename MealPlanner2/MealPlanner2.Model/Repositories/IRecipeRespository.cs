﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MealPlanner2.Model.Repositories
{
    public interface IRecipeRespository
    {
        void addRecipe(Recipe recipe);
        void deleteRecipe(Recipe recipe);
        List<Recipe> getRecipes();
        void editRecipe(Recipe recipe);
        int getNewID();
        Recipe getRecipeById(int id);
        List<Recipe> getRecipeByIds(List<int> ids);
        List<Recipe> getVisibleRecipes(User user);
       
    }
}
