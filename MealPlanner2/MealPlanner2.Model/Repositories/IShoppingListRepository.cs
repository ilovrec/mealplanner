﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MealPlanner2.Model.Repositories
{
    public interface IShoppingListRepository
    {
        void addShoppingList(ShoppingList list);
        void deleteShoppingList(ShoppingList list);
        void deleteShoppingList(int id);
        List<ShoppingList> getShoppingLists();
        List<ShoppingList> getMyShoppingLists(User user);
        void editShoppingList(ShoppingList list);
        ShoppingList getShoppingListById(int id);
        int getNewID();

    }
}
