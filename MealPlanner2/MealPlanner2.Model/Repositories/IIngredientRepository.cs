﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MealPlanner2.Model;

namespace MealPlanner2.Model.Repositories
{
    public interface IIngredientRepository
    {
        void addIngredient(Ingredient i);
        void deleteIngredient(Ingredient i);

        List<Ingredient> getAllIngredients();
        int getNewID();

    }
}
