﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MealPlanner2.Model.Repositories
{
    public interface IWeeklyPlanRepository
    {
        void addWeeklyPlan(WeeklyPlan weeklyPlan);
        void deleteWeeklyPlan(WeeklyPlan weeklyPlan);
        void deleteWeeklyPlan(int id);
        List<WeeklyPlan> getWeeklyPlans();
        void editWeeklyPlan(WeeklyPlan weeklyPlan);
        List<WeeklyPlan> getVisibleWeeklyPlans(User user);
        WeeklyPlan getWeeklyPlanById(int id);
        int getNewID();
    }
}
