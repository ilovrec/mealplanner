﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MealPlanner2.Model.Repositories
{
    public interface IUserRepository
    {
        void addUser(User User);
        List<User> getUsers();
        int getNewID();
        User getUserByUsername(string name);
        User getUserById(int id);
    }
}
