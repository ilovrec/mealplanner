﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MealPlanner2.Model.Repositories
{
    public interface IDailyPlanRepository
    {
        void addDailyPlan(DailyPlan dailyPlan);
        void deleteDailyPlan(DailyPlan dailyPlan);
        void deleteDailyPlan(int id);
        List<DailyPlan> getDailyPlans();
        void editDailyPlan(DailyPlan dailyPlan);
        int getNewID();
    }
}
