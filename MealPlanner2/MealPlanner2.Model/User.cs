﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MealPlanner2.Model
{
    public class User
    {
     
        public User() { }
        public User(string username, string password, string email, int id)
        {
            Username = username;
            Password = password;
            Id = id;

        }

        public User(string username, string password)
        {
            Username = username;
            Password = password;
        }

        

        
        public virtual string Username { get; set; }
        public virtual string Password { get; set; }
        public virtual int Id { get; protected set; }
        public virtual IList<Recipe> Recipes{ get; protected set; }
    }
}
