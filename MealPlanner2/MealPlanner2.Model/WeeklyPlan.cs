﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace MealPlanner2.Model
{
    public class WeeklyPlan
    {
        public WeeklyPlan(DailyPlan[] plans,int id, DateTime monday, User user)
        {
            PlansArray = plans;
            Id = id;
            Monday = monday;
            Owner = user;
        }

        public WeeklyPlan(DailyPlan[] plans,  DateTime monday, string username)
        {
            PlansArray = plans.ToList();
            Monday = monday;
            Username= username;
        }

        public WeeklyPlan()
        {
            PlansArray = new List<DailyPlan>();
        }

        public virtual void AddDailyPlan(DailyPlan plan)
        {
            plan.WeeklyPlan = this;
            PlansArray.Add(plan);
        }

        public virtual void deleteDailyPlans()
        {
            PlansArray = new List<DailyPlan>();
        }

        public static DateTime getMonday(DateTime date)
        {
            var now = date;
            var firstDayOfWeek = Thread.CurrentThread.CurrentCulture.DateTimeFormat.FirstDayOfWeek;
            if (now.DayOfWeek == DayOfWeek.Sunday && firstDayOfWeek != DayOfWeek.Sunday)
                now = now.AddDays(-1);
            DateTime monday = now.AddDays(-(now.DayOfWeek - DayOfWeek.Monday));
            return monday;
        }

        public virtual IList<DailyPlan> PlansArray { get;  set; }
        public virtual  int Id { get; protected set; }
        public virtual DateTime Monday { get; set; }
        public virtual User Owner { get; set; }
        public virtual string Username { get; set; }
    }
}
