﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MealPlanner2.Model;

namespace Model.Repositories
{
    class UserRespository
    {
        private readonly List<User> _listUsers = new List<User>();
        private static UserRespository _instance;

        private UserRespository() 
        {
         
        }

        public static UserRespository getInstance()
        {
            return _instance ?? (_instance = new UserRespository());
        }

        public User getByUsername(string username) 
        {
            var user = (from l in _listUsers where l.Username == username select l).First();

            if (user != null)
                return user;
            return null;
        }

        public void addUser(User inUser)
        {
            if (!_listUsers.Any(u => u.Username == inUser.Username))
                _listUsers.Add(inUser);
        }

    }
}
