﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MealPlanner2.Model
{
    public class Recipe
    {
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }
        public virtual string Category { get; set; }
        public virtual int Portions { get; set; }
        public virtual int Id { get; protected set; }
        public virtual int Preparation { get; set; }
        public virtual IList<Ingredient> Ingredients { get; protected set; }
        public virtual User Owner { get; set; }
        public virtual string Username { get; set; }
        public virtual bool IsPublic { get; set; }
        public virtual DailyPlan DailyPlan { get; set; }

        public Recipe() {
            Ingredients = new List<Ingredient>();
        }

        public virtual void AddIngredient(Ingredient ingredient)
        {
            ingredient.Recipe = this;
            Ingredients.Add(ingredient);
        }

        public Recipe(string name, string description, decimal preparation, decimal portions, List<Ingredient> ing, string category, int id, bool isPublic, User owner)
        {
            Name = name;
            Description =description;
            Ingredients = ing;
            Preparation = Convert.ToInt32(preparation);
            Portions = Convert.ToInt32(portions);
            checkAndSetCategory(category);
            Id = id;
            Owner = owner;
            IsPublic = isPublic;

        }

        public Recipe(string name, string description, decimal preparation, decimal portions, List<Ingredient> ing, string category, bool isPublic, User owner)
        {
            Name = name;
            Description = description;
            Ingredients = ing;
            Preparation = Convert.ToInt32(preparation);
            Portions = Convert.ToInt32(portions);
            checkAndSetCategory(category);
            Owner = owner;
            IsPublic = isPublic;

        }

        public Recipe(string name, string description, decimal preparation, decimal portions, List<Ingredient> ing, string category, bool isPublic, string username)
        {
            Name = name;
            Description = description;
            Ingredients = ing;
            Preparation = Convert.ToInt32(preparation);
            Portions = Convert.ToInt32(portions);
            checkAndSetCategory(category);
            IsPublic = isPublic;
            Username = username;

        }

        public virtual void checkAndSetCategory(string category)
        {
            List<String> allowedCategories = new List<string> { "Glavno jelo s mesom", "Vegetarijansko glavno jelo", "Juha", "Salata", "Prilog", "Desert", "Međuobrok", "Neodređeno" };
            if (allowedCategories.Find(x => x.Equals(category)) != null) {
                Category = category;
            }
            else throw new ArgumentException();
        }



        public virtual string getIngredientsAsString()
        {
            List<string> strings = Ingredients.Select(s => (string)s.Name + ": " + s.Quantity + " " + s.MeasureUnit).ToList();
            return String.Join(Environment.NewLine, strings.ToArray());
        }

        public  static string getRecipeListAsString(List<Recipe> recipes)
        {
            List<string> strings = recipes.Select(s => (string)s.Name ).ToList();
            return String.Join(", ", strings.ToArray());
        }

       

    }
}
