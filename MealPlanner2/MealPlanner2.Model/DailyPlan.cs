﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MealPlanner2.Model
{
    public class DailyPlan
    {
        public DailyPlan()
        {
            Breakfast = new List<int>();
            Dinner = new List<int>();
            Lunch = new List<int>();
        }

        public virtual void AddBreakfast(Recipe breakfast)
        {
            Breakfast.Add(breakfast.Id);
        }

        public virtual void AddLunch(Recipe lunch)
        {
            Lunch.Add(lunch.Id);
        }


        public virtual void AddDinner(Recipe dinner)
        {
            Dinner.Add(dinner.Id);
        }

        public virtual void SetBreakfast(List<int> recipes)
        {
            Breakfast= recipes;
        }

        public virtual void SetLunch(List<int> recipes)
        {
            Lunch = recipes;
        }

        public virtual void SetDinner(List<int> recipes)
        {
            Dinner = recipes;
        }


        public DailyPlan(List<Recipe> breakfast, List<Recipe> lunch, List<Recipe> dinner, int id, int dayOfWeek)
        {
            Breakfast = new List<int>();
            Dinner = new List<int>();
            Lunch = new List<int>();
            Id = id;
            DayOfWeek = dayOfWeek;
        }

        public DailyPlan(List<Recipe> breakfast, List<Recipe> lunch, List<Recipe> dinner, int dayOfWeek)
        {
            Breakfast = new List<int>();
            Dinner = new List<int>();
            Lunch = new List<int>();
            DayOfWeek = dayOfWeek;
        }
        public DailyPlan( int dayOfWeek)
        {
            Breakfast = new List<int>();
            Dinner = new List<int>();
            Lunch = new List<int>();
            DayOfWeek = dayOfWeek;
        }

        public virtual string getDayName()
        {
            switch (DayOfWeek)
            {
                case 0: return "Ponedjeljak";
                case 1: return "Utorak";
                case 2: return "Srijeda";
                case 3: return "Četvrtak";
                case 4: return "Petak";
                case 5: return "Subota";
                case 6: return "Nedjelja";
            }
            throw new IndexOutOfRangeException();     
        }

        public virtual IList<int> Breakfast { get; protected set; }
        public virtual IList<int> Lunch { get; protected set; }
        public virtual IList<int> Dinner { get; protected set; }
        public virtual int Id { get; protected set; }
        public virtual int DayOfWeek { get; set; }

        public virtual WeeklyPlan WeeklyPlan { get; set; }

    }
}
