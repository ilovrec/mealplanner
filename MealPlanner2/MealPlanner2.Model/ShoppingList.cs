﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MealPlanner2.Model
{
    public class ShoppingList
    {
        public ShoppingList() {
            Ingredients = new List<Ingredient>();
        }

        public ShoppingList(string name, List<Ingredient> ing, int id, User user)
        {
            Name = name;
            Ingredients = ing;
            ModifiedAt = DateTime.Now;
            Id = id;

        }

        public ShoppingList(string name, List<Ingredient> ing,  string username)
        {
            Name = name;
            Ingredients = ing;
            ModifiedAt = DateTime.Now;
            Username = username;

        }

        public virtual void AddIngredient(Ingredient ingredient)
        {
            ingredient.ShoppingList = this;
            Ingredients.Add(ingredient);
        }


        public virtual string getIngredientsOneLine()
        {
            List<string> strings = Ingredients.Select(s => (string)s.Name + ": " + s.Quantity + " " + s.MeasureUnit).ToList();
            return String.Join(",", strings.ToArray());
        }

        public virtual string getIngredientsMultipleLines()
        {
            List<string> strings = Ingredients.Select(s => (string)s.Name + ": " + s.Quantity + " " + s.MeasureUnit).ToList();
            return String.Join(Environment.NewLine, strings.ToArray());
        }

        

        public virtual string Name { get; set; }
        public virtual IList<Ingredient> Ingredients { get; protected set; }
        public virtual string Username { get; set; }
        public virtual DateTime ModifiedAt { get; set; }
        public virtual int Id { get; protected set; }
    }
}
