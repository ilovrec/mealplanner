﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MealPlanner2.Model
{
    public class Ingredient
    {
        public virtual int Id { get; protected set; }
        public virtual string Name { get; set; }
        public virtual int Quantity { get; set; }
        public virtual string MeasureUnit { get; set; }
        public virtual Recipe Recipe { set; get; }
        public virtual ShoppingList ShoppingList { set; get; }

        public Ingredient(string name, decimal quantity, string measure_unit, int id)
        {
            Name = name;
            Quantity = Convert.ToInt32(quantity);
            MeasureUnit = measure_unit;
            Id = id;

        }

        public Ingredient(string name, decimal quantity, string measure_unit)
        {
            Name = name;
            Quantity = Convert.ToInt32(quantity);
            MeasureUnit = measure_unit;

        }

        public static string getIngredientsNames(List<Ingredient> ingredients)
        {
            List<string> strings = ingredients.Select(s => (string)s.Name ).ToList();
            return String.Join(", ", strings.ToArray());
        }

        public Ingredient() { }


    }
}
