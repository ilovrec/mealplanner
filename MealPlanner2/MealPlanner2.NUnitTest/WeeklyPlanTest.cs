﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using MealPlanner2.Model;

namespace MealPlanner2.NUnitTest
{
    [TestFixture]
    public class WeeklyPlanTest
    {
        WeeklyPlan plan;

        [SetUp]
        public void Init()
        {
            plan = new WeeklyPlan();
        }

        [Test]
        public void NUnit_AddDailyPlanToWeeklyPlan()
        {
            DailyPlan day = new DailyPlan();
            day.DayOfWeek = 0;
            day.Breakfast.Add(1);
            day.Dinner.Add(2);
            plan.AddDailyPlan(day);
            Assert.AreEqual(plan.PlansArray[0], day);
        }

        [Test]
        public void NUnit_getMonday()
        {
            DateTime date = new DateTime(2020, 2, 1);
            DateTime expectedMonday = new DateTime(2020, 1,27 );
            DateTime monday = WeeklyPlan.getMonday(date);
            Assert.AreEqual(expectedMonday,monday);
        }
    }
}
