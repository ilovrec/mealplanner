﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using MealPlanner2.Model;

namespace MealPlanner2.NUnitTest
{
    [TestFixture]
    public class DailyPlanTest
    {
        DailyPlan plan;
        DailyPlan planWrongDayIndex;

        [SetUp]
        public void Init()
        {
            plan = new DailyPlan(null, null, null, 0);
            planWrongDayIndex = new DailyPlan(null, null, null, 10);
        }

        [Test]
        public void NUnit_getDayName_GoodIndex()
        {
            String name = plan.getDayName();
            Assert.AreEqual("Ponedjeljak", name);
        }

        [Test]
        public void NUnit_getDayName_BadIndex()
        {
            Assert.Throws<IndexOutOfRangeException>(
                delegate
                {
                    planWrongDayIndex.getDayName();
                });
        }

        [Test]
        public void NUnit_AddBreakfast()
        {
            Recipe recipe = new Recipe("Recept", "opis", 35, 4, new List<Ingredient>(), "Salata", true, "Anonymous");
            plan.AddBreakfast(recipe);
            Assert.AreEqual(plan.Breakfast.First(), recipe.Id);
        }

        [Test]
        public void NUnit_WeeklyPlanInDailyPlan()
        {
            WeeklyPlan week = new WeeklyPlan();
            week.AddDailyPlan(plan);
            Assert.AreEqual(week, plan.WeeklyPlan);
        }
    }
}
