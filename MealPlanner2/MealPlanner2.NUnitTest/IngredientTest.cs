﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using MealPlanner2.Model;

namespace MealPlanner2.NUnitTest
{
    [TestFixture]
    public class IngredientTest
    {
        List<Ingredient> ingredients;

        [SetUp]
        public void Init()
        {
            ingredients = new List<Ingredient>();
            ingredients.Add(new Ingredient("Fanta", 2, "L"));
            ingredients.Add(new Ingredient("Cola", 1, "L"));
            ingredients.Add(new Ingredient("Sir", 20, "dg"));
        }

        [Test]
        public void MsUnit_getIngredientNames()
        {
            String names = Ingredient.getIngredientsNames(ingredients);

            Assert.AreEqual("Fanta, Cola, Sir", names);
        }

        [Test]
        public void NUnit_RecipeInIngredient()
        {
            Recipe recipe = new Recipe("Recept", "opis", 35, 4, new List<Ingredient>(), "Salata", true, "Anonymous");
            recipe.AddIngredient(ingredients[0]);
            Assert.AreEqual(ingredients[0].Recipe, recipe);
        }

        [Test]
        public void NUnit_ShoppingListInIngredient()
        {
            ShoppingList list = new ShoppingList("moj popis", new List<Ingredient>(),"Anonymuos");
            list.AddIngredient(ingredients[0]);
            Assert.AreEqual(ingredients[0].ShoppingList, list);
        }

        
    }
}
