﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using MealPlanner2.Model;

namespace MealPlanner2.NUnitTest
{
    [TestFixture]
    public class RecipeTest
    {
        Recipe recipe;

        [SetUp]
        public void Init()
        {
            recipe = new Recipe("Recept", "opis", 35, 4, new List<Ingredient>(), "Salata", true, "Anonymous");
        }

        [Test]
        public void NUnit_AddIngredient()
        {
            Ingredient ing = new Ingredient("Cola", 1, "L");
            recipe.AddIngredient(ing);

            Assert.AreEqual(recipe.Ingredients.FirstOrDefault(), ing);
        }

        [Test]
        public void NUnit_getRecipeIngredientsAsString()
        {
            Ingredient ing = new Ingredient("Cola", 1, "L");
            Ingredient ing2 = new Ingredient("Fanta", 2, "L");
            recipe.AddIngredient(ing);
            recipe.AddIngredient(ing2);

            String ings = recipe.getIngredientsAsString();
            String expected = "Cola: 1 L\r\nFanta: 2 L";
            Assert.AreEqual(expected, ings);
        }

        [Test]
        public void NUnit_GetRecipeNamesAsString()
        {
            Recipe recipe2 = new Recipe("Recept2", "opis", 35, 4, new List<Ingredient>(), "Salata", true, "Anonymous");
            Recipe recipe3 = new Recipe("Novi recept", "opis", 35, 4, new List<Ingredient>(), "Salata", true, "Anonymous");

            List<Recipe> recipes = new List<Recipe> { recipe, recipe2, recipe3 };
            String recipeNames = Recipe.getRecipeListAsString(recipes);

            Assert.AreEqual("Recept, Recept2, Novi recept", recipeNames);
        }

        [Test]
        public void NUnit_AddRecipeWrongCategory()
        {
            Assert.Throws<ArgumentException>(
                delegate
                {
                    Recipe recipe2 = new Recipe("Recept2", "opis", 35, 4, new List<Ingredient>(), "Salata2", true, "Anonymous");
                });
        }


    }
}
