﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using MealPlanner2.Model;

namespace MealPlanner2.NUnitTest
{
    [TestFixture]
    public class ShoppingListTest
    {
        ShoppingList list;

        [SetUp]
        public void Init()
        {
           list = new ShoppingList("moj popis", new List<Ingredient>(), "Anonymuos");
        }

        [Test]
        public void NUnit_AddIngredient()
        {
            Ingredient ing = new Ingredient("Cola", 1, "L");
            list.AddIngredient(ing);
            Assert.AreEqual(list.Ingredients.FirstOrDefault(), ing);
        }

        [Test]
        public void NUnit_GetIngredientsInLines()
        {
            Ingredient ing = new Ingredient("Cola", 1, "L");
            Ingredient ing2 = new Ingredient("Fanta", 2, "L");
            list.AddIngredient(ing);
            list.AddIngredient(ing2);
            String expected = "Cola: 1 L\r\nFanta: 2 L";
            Assert.AreEqual(list.getIngredientsMultipleLines(), expected);
        }
    }
}
