﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MealPlanner2.Model;
using MealPlanner2.Model.Repositories;

namespace MealPlanner2.MemoryBasedDAL
{
    public class IngredientRepository: IIngredientRepository
    {
        private readonly List<Ingredient> _listIngredients = new List<Ingredient>();
        private static IngredientRepository _instance;
        private static int _nextID = 1;

        private IngredientRepository()
        {
        }

        public int getNewID()
        {
            int nextID = _nextID;
            _nextID++;
            return nextID;
        }

        public static IngredientRepository getInstance()
        {
            return _instance ?? (_instance = new IngredientRepository());
        }

        /*public Ingredient getByName(string Ingredientname)
        {
            var Ingredient = (from l in _listIngredients where l.Ingredientname == Ingredientname select l).First();

            if (Ingredient != null)
                return Ingredient;
            return null;
        } */

        public void addIngredient(Ingredient inIngredient)
        {
             _listIngredients.Add(inIngredient);

        }

        public void deleteIngredient(Ingredient i)
        {

        }

        public List<Ingredient> getAllIngredients()
        {
            return _listIngredients;
        }
    }
}
