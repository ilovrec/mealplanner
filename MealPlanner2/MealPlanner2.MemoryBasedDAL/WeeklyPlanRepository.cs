﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MealPlanner2.Model;
using MealPlanner2.Model.Repositories;

namespace MealPlanner2.MemoryBasedDAL
{
    public class WeeklyPlanRepository: IWeeklyPlanRepository
    {
        private readonly List<WeeklyPlan> _listWeeklyPlans = new List<WeeklyPlan>();
        private static WeeklyPlanRepository _instance;
        private static int _nextID = 1;
        private static int _nextIDDaily = 1;
        private RecipeRepository recipeRepo= RecipeRepository.getInstance();

        private WeeklyPlanRepository()
        {
            User user = UserRepository.getInstance().getUserByUsername("admin");
            List<Recipe>  all_recipes = recipeRepo.getRecipes();
            List<Recipe> recipes1 = new List<Recipe>();
            recipes1.Add(all_recipes[0]);
            List<Recipe> recipes2 = new List<Recipe>();
            recipes1.Add(all_recipes[1]);
            List<Recipe> recipes3 = new List<Recipe>();
            recipes1.Add(all_recipes[2]);
            DailyPlan day1 = new DailyPlan(recipes1, recipes2, recipes3, -1, 0);
            DailyPlan[] dailyArray =new DailyPlan[7];
            dailyArray[1] = day1;
            _listWeeklyPlans.Add(new WeeklyPlan(dailyArray, -1, new DateTime(2020, 1, 21), user));
        }
        public static WeeklyPlanRepository getInstance()
        {
            return _instance ?? (_instance = new WeeklyPlanRepository());
        }

        public int getNewID()
        {
            int nextID = _nextID;
            _nextID++;
            return nextID;
        }

        public int getNewIDaily()
        {
            int nextIDDaily = _nextIDDaily;
            _nextIDDaily++;
            return nextIDDaily;
        }

        public void addWeeklyPlan(WeeklyPlan weeklyPlan)
        {
            _listWeeklyPlans.Add(weeklyPlan);
        }

        public void deleteWeeklyPlan(WeeklyPlan weeklyPlan)
        {
            throw new NotImplementedException();
        }

        public void deleteWeeklyPlan(int id)
        {
            _listWeeklyPlans.RemoveAll(chunk => chunk.Id == id);
        }

        public List<WeeklyPlan> getWeeklyPlans()
        {
            return _listWeeklyPlans;
        }

        public void editWeeklyPlan(WeeklyPlan weeklyPlan)
        {
            deleteWeeklyPlan(weeklyPlan.Id);
            addWeeklyPlan(weeklyPlan);
        }

        public WeeklyPlan getWeeklyPlanByName(string name)
        {
            throw new NotImplementedException();
        }

        public WeeklyPlan getWeeklyPlanByDate(DateTime date)
        {
            throw new NotImplementedException();
        }

        public List<WeeklyPlan> getVisibleWeeklyPlans(User user)
        {
            return _listWeeklyPlans.FindAll(i => i.Owner == user);
        }

        public WeeklyPlan getWeeklyPlanById(int id)
        {
            return _listWeeklyPlans.Find(chunk => chunk.Id == id);
        }
    }
}
