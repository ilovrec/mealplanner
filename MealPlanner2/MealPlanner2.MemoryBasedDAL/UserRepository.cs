﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MealPlanner2.Model.Repositories;
using MealPlanner2.Model;

namespace MealPlanner2.MemoryBasedDAL
{
    public class UserRepository : IUserRepository
    {
        private readonly List<User> _listUsers = new List<User>();
        private static UserRepository _instance;
        private static int _nextID = 1;

        public static UserRepository getInstance()
        {
            return _instance ?? (_instance = new UserRepository());
        }

        public int getNewID()
        {
            int nextID = _nextID;
            _nextID++;
            return nextID;
        }
        public void addUser(User user)
        {
            _listUsers.Add(user);
        }

        public void deleteUser(User user)
        {
            _listUsers.Remove(user);
        }

        public void editUser(User user)
        {
            throw new NotImplementedException();
        }

        public List<User> getUsers()
        {
            return _listUsers;
        }

        public User getUserByUsername(string name)
        {
            return _listUsers.Find(i => i.Username == name);
        }

        public User getUserById(int id)
        {
            return _listUsers.Find(i => i.Id == id);
        }
    }
}
