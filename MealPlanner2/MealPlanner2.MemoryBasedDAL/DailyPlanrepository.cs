﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MealPlanner2.Model;
using MealPlanner2.Model.Repositories;

namespace MealPlanner2.MemoryBasedDAL
{
    public class DailyPlanRepository : IDailyPlanRepository
    {
        private List<DailyPlan> _listDailyPlans = new List<DailyPlan>();
        private static DailyPlanRepository _instance;
        private static int _nextID = 1;

        public DailyPlanRepository() 
        {

        }

        public static DailyPlanRepository getInstance()
        {
            return _instance ?? (_instance = new DailyPlanRepository());
        }

        public int getNewID()
        {
            int nextID = _nextID;
            _nextID++;
            return nextID;
        }
        public void addDailyPlan(DailyPlan dailyPlan)
        {
            throw new NotImplementedException();
        }

        public void deleteDailyPlan(DailyPlan dailyPlan)
        {
            throw new NotImplementedException();
        }

        public void deleteDailyPlan(int id)
        {
            throw new NotImplementedException();
        }

        public void editDailyPlan(DailyPlan dailyPlan)
        {
            throw new NotImplementedException();
        }

        public DailyPlan getDailyPlanByDate(DateTime date)
        {
            throw new NotImplementedException();
        }

        public DailyPlan getDailyPlanByName(string name)
        {
            throw new NotImplementedException();
        }

        public List<DailyPlan> getDailyPlans()
        {
            throw new NotImplementedException();
        }

        
    }
}
