﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MealPlanner2.Model.Repositories;
using MealPlanner2.Model;

namespace MealPlanner2.MemoryBasedDAL
{
    public class ShoppingListRepository : IShoppingListRepository
    {
        private readonly List<ShoppingList> _listShoppingLists = new List<ShoppingList>();
        private static ShoppingListRepository _instance;
        private static IngredientRepository ingRepo = IngredientRepository.getInstance();
        private static int _nextID = 1;

        private ShoppingListRepository()
        {
            User user = new User("admin", "admin", "", 0);
            UserRepository.getInstance().addUser(user);
            Ingredient ing = new Ingredient("kola", 1, "L", ingRepo.getNewID());
            List<Ingredient> list1 = new List<Ingredient>();
            List<Ingredient> list2 = new List<Ingredient>();
            List<Ingredient> list3 = new List<Ingredient>();

            list1.Add(ing);
            list2.Add(ing);
            ShoppingList dummy_list = new ShoppingList("moj popis", list1, -3, user );
            _listShoppingLists.Add(dummy_list);
            list2.Add(new Ingredient("Šunka", 10, "dg", ingRepo.getNewID()));
            list3.Add(new Ingredient("Šunka", 10, "dg", ingRepo.getNewID()));
            dummy_list = new ShoppingList("popis2", list2, -1, user);
            _listShoppingLists.Add(dummy_list);
            list2.Add(new Ingredient("prsut", 15, "dg", ingRepo.getNewID()));
            list3.Add(new Ingredient("prsut", 15, "dg", ingRepo.getNewID()));
            dummy_list = new ShoppingList("popis3", list3, -2, user);
            _listShoppingLists.Add(dummy_list);
        }

        public static ShoppingListRepository getInstance()
        {
            return _instance ?? (_instance = new ShoppingListRepository());
        }
        public void addShoppingList(ShoppingList shopList)
        {
           
            _listShoppingLists.Add(shopList);
        }

        public void deleteShoppingList(ShoppingList shopList)
        {
            _listShoppingLists.Remove(shopList);
        }

        public void editShoppingList(ShoppingList shopList)
        {
            shopList.ModifiedAt = DateTime.Now;
            deleteShoppingList(shopList.Id);
            addShoppingList(shopList);
        }

        public int getNewID()
        {
            int nextID = _nextID;
            _nextID++;
            return nextID;
        }

        public List<ShoppingList> getShoppingLists()
        {
            return _listShoppingLists;
        }

        public ShoppingList getByNameShoppingList(string name)
        {
           return _listShoppingLists.Find(i => i.Name == name);
        }

        public void deleteShoppingList(int id)
        {
           _listShoppingLists.RemoveAll(chunk => chunk.Id == id);
        }

        public List<ShoppingList> getMyShoppingLists(User user)
        {
          return _listShoppingLists.FindAll(i => i.Username == user.Username);
        }

        public ShoppingList getShoppingListById(int id)
        {
            return _listShoppingLists.Find(i => i.Id == id);
        }
    }
}
