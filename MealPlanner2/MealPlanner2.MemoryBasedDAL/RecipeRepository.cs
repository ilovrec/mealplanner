﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MealPlanner2.Model.Repositories;
using MealPlanner2.Model;

namespace MealPlanner2.MemoryBasedDAL
{
    public class RecipeRepository : IRecipeRespository
    {
        private readonly List<Recipe> _listRecipes = new List<Recipe>();
        private static RecipeRepository _instance;
        private static int _nextID = 1;

        private RecipeRepository()
        {
            Ingredient ing = new Ingredient("kola", 1, "L", IngredientRepository.getInstance().getNewID());
            List<Ingredient> list1 = new List<Ingredient>();
            List<Ingredient> list2 = new List<Ingredient>();
            List<Ingredient> list3 = new List<Ingredient>();

            list1.Add(ing);
            list2.Add(ing);
            list2.Add(new Ingredient("Šunka", 10, "dg", IngredientRepository.getInstance().getNewID()));
            list3.Add(new Ingredient("Šunka", 10, "dg", IngredientRepository.getInstance().getNewID()));
           
            list2.Add(new Ingredient("prsut", 15, "dg", IngredientRepository.getInstance().getNewID()));
            list3.Add(new Ingredient("prsut", 15, "dg", IngredientRepository.getInstance().getNewID()));
            Ingredient ing1 = new Ingredient("kola", 1, "L", IngredientRepository.getInstance().getNewID());
            Ingredient ing2 = new Ingredient("sunka", 10, "dg", IngredientRepository.getInstance().getNewID());
            Ingredient ing3 = new Ingredient("fanta", 5, "mL", IngredientRepository.getInstance().getNewID());
            
            _listRecipes.Add(new Recipe("meso s umakom", "super fino jelo, mala pripremam, definitivno radi, jebenica", 60, 5,list1, "Juha", getNewID(),true, null ));
            _listRecipes.Add(new Recipe("recept2", "njam njam njam njam super fino jelo, mala pripremam, definitivno radi, jebenica", 35, 2, list2, "Salata", getNewID(), true, null));
            _listRecipes.Add(new Recipe("Falafel", " definitivno radi, jebenica, super duper ekstra smeksi keksi", 67, 5, list1, "Vegetarijansko glavno jelo", getNewID(), true, null));
        }

        public static RecipeRepository getInstance()
        {
            return _instance ?? (_instance = new RecipeRepository());
        }

        public int getNewID()
        {
            int nextID = _nextID;
            _nextID++;
            return nextID;
        }
        public void addRecipe(Recipe recipe)
        {
            _listRecipes.Add(recipe);
        }

        public void deleteRecipe(Recipe recipe)
        {
            _listRecipes.Remove(recipe);
        }

        public void editRecipe(Recipe recipe)
        {
            throw new NotImplementedException();
        }

        public List<Recipe> getRecipes()
        {
            return _listRecipes;
        }

        public Recipe getRecipeByName(string name)
        {
            return _listRecipes.Find(i => i.Name == name);
        }

        public Recipe getRecipeById(int id)
        {
            return _listRecipes.Find(i => i.Id == id);
        }

        public List<Recipe> getVisibleRecipes(User user)
        {
            List<Recipe> result = new List<Recipe>();
            result.AddRange(_listRecipes.FindAll(i => i.IsPublic == true));
            result.AddRange(_listRecipes.FindAll(i => i.Owner == user).Except(result));
            return result;
        }

        public void addRecipe(Recipe recipe, User owner)
        {
            throw new NotImplementedException();
        }

        public void addRecipe(Recipe recipe, Ingredient ing)
        {
            throw new NotImplementedException();
        }

        public List<Recipe> getRecipeByIds(List<int> ids)
        {
            List<Recipe> result = new List<Recipe>();
            foreach (var id in ids)
            {
                result.Add(_listRecipes.Where(i => i.Id == id).First());
            }
            return result;
        }
    }
}
