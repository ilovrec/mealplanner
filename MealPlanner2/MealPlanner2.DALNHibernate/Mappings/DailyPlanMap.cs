﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using MealPlanner2.Model;

namespace MealPlanner2.DALNHibernate.Mappings
{
    public class DailyPlanMap: ClassMap<DailyPlan>
    {
        public DailyPlanMap()
        {
            Id(x => x.Id);
            Map(x => x.DayOfWeek);
            HasMany(x => x.Breakfast).Element("BreakfastId").AsBag().Not.LazyLoad();
            HasMany(x => x.Lunch).Element("LunchId").AsBag().Not.LazyLoad();
            HasMany(x => x.Dinner).Element("DinnerId").AsBag().Not.LazyLoad();

            References<WeeklyPlan>(x => x.WeeklyPlan);
        }
    }
}
