﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using MealPlanner2.Model;

namespace MealPlanner2.DALNHibernate.Mappings
{
    public class RecipeMap: ClassMap<Recipe>
    {
        public RecipeMap()
        {
            Id(x => x.Id);
            Map(x => x.Name);
            Map(x => x.Category);
            Map(x => x.Preparation);
            Map(x => x.Portions);
            Map(x => x.Description);
            Map(x => x.IsPublic);
            Map(x => x.Username);
            HasMany<Ingredient>(x => x.Ingredients).Cascade.SaveUpdate().Not.LazyLoad();
        }
    }
}
