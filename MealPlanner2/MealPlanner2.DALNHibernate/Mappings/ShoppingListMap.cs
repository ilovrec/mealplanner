﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using MealPlanner2.Model;

namespace MealPlanner2.DALNHibernate.Mappings
{
    public class ShoppingListMap: ClassMap<ShoppingList>
    {
        public ShoppingListMap()
        {
            Id(x => x.Id);
            Map(x => x.Name);
            Map(x => x.Username);
            Map(x => x.ModifiedAt);
            HasMany<Ingredient>(x => x.Ingredients).Cascade.SaveUpdate().Not.LazyLoad().Cascade.Delete();
        }
    }
}
