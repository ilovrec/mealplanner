﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using MealPlanner2.Model;

namespace MealPlanner2.DALNHibernate.Mappings
{
    public class WeeklyPlanMap: ClassMap<WeeklyPlan>
    { 
        public WeeklyPlanMap()
        {
            Id(x => x.Id);
            Map(x => x.Monday);
            Map(x => x.Username);
            HasMany<DailyPlan>(x => x.PlansArray).Cascade.SaveUpdate().Not.LazyLoad().Cascade.Delete();
        }
    }
}
