﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MealPlanner2.DALNHibernate.Mappings;
using MealPlanner2.Model;

using NHibernate;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using FluentNHibernate.Conventions.Helpers;
using FluentNHibernate.Automapping;

namespace MealPlanner2.DALNHibernate.NHibernateHelper
{
    public class NhibernateService
    {
        private static ISessionFactory _sessionFactory;

        public static ISession OpenSession()
        {
            try
            {
                if (_sessionFactory == null)
                {
                    _sessionFactory = OpenSessionFactory();
                }
                ISession session = _sessionFactory.OpenSession();
                return session;
            }
            catch (Exception e)
            {
                throw e.InnerException ?? e;
            }
        }

        public static ISessionFactory OpenSessionFactory()
        {
            var fluentConfig = Fluently.Configure()
                                .Database(SQLiteConfiguration.Standard
                                            .ConnectionString("Data Source=Workouts.db;Version=3")
                                            .AdoNetBatchSize(100))
                                .Mappings(m =>
                                {
                                    m.FluentMappings.Add<IngredientMap>();
                                    m.FluentMappings.Add<RecipeMap>();
                                    m.FluentMappings.Add<UserMap>();
                                    m.FluentMappings.Add<ShoppingListMap>();
                                    m.FluentMappings.Add<WeeklyPlanMap>();
                                    m.FluentMappings.Add<DailyPlanMap>();
                                    
                                });

            var nhCfg = fluentConfig.BuildConfiguration();
            _sessionFactory = nhCfg.BuildSessionFactory();
           //var schemaExport = new SchemaExport(nhCfg);
           //schemaExport.Create(false, true);  

            return _sessionFactory;
        }

    }
}
