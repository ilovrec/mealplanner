﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MealPlanner2.Model;
using MealPlanner2.Model.Repositories;
using NHibernate;

namespace MealPlanner2.DALNHibernate.Repositories
{
    public class NHUserRepository:IUserRepository
    {
        private IList<User> _listUsers = new List<User>();
        private static NHUserRepository _instance;

        public static NHUserRepository getInstance()
        {
            if (_instance == null)
            {
                _instance = new NHUserRepository();
                _instance.AddAnonymus();
            }

            return _instance;
        }

        private void AddAnonymus()
        {
            User anon = new User("Anonimni korisnik", "");
            addUser(anon);
        }

        private void LoadUsersFromDatabase()
        {
            using (ISession session = NHibernateHelper.NhibernateService.OpenSession())
            {
                IQuery query = session.CreateQuery(
                    "from User as u order by u.Username asc");
                _listUsers = query.List<User>();
            }


        }

        public void addUser(User User)
        {
            using (ISession session = NHibernateHelper.NhibernateService.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    session.Save(User);
                    transaction.Commit();
                }
            }

            LoadUsersFromDatabase();
        }

        public IList<User> getUsers()
        {
            LoadUsersFromDatabase();
            return _listUsers;
        }

        public int getNewID()
        {
            throw new NotImplementedException();
        }

        public User getUserByUsername(string name)
        {
            LoadUsersFromDatabase();
            return _listUsers.Where(i => i.Username == name).FirstOrDefault();
        }

        public User getUserById(int id)
        {
            throw new NotImplementedException();
        }

        List<User> IUserRepository.getUsers()
        {
            throw new NotImplementedException();
        }
    }
    
}
