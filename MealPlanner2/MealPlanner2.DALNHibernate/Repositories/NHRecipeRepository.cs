﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MealPlanner2.Model;
using MealPlanner2.Model.Repositories;
using NHibernate;
using MealPlanner2.MemoryBasedDAL;

namespace MealPlanner2.DALNHibernate.Repositories
{
    public class NHRecipeRepository: IRecipeRespository
    {
        private static NHRecipeRepository _nhRecipeRepository;
        private IList<Recipe> _listRecipes = new List<Recipe>();

        public static NHRecipeRepository GetInstance()
        {
            if (_nhRecipeRepository == null)
            {
                _nhRecipeRepository = new NHRecipeRepository();
                _nhRecipeRepository.fillRecipeRepo();
            }

            return _nhRecipeRepository;
        }
        private void LoadRecipesFromDatabase()
        {
            using (ISession session = NHibernateHelper.NhibernateService.OpenSession())
            {
                IQuery query = session.CreateQuery(
                    "from Recipe as rec order by rec.Name asc");
                _listRecipes = query.List<Recipe>();
            }
        }



        public void addRecipe(Recipe recipe, Ingredient ing)
        {
            using (ISession session = NHibernateHelper.NhibernateService.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    session.Save(recipe);
                    transaction.Commit();
                }
            }

        }

        public void deleteRecipe(Recipe recipe)
        {
            using (ISession session = NHibernateHelper.NhibernateService.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    session.Delete(recipe);
                    transaction.Commit();
                }
            }
        }

        public void editRecipe(Recipe recipe)
        {
            using (ISession session = NHibernateHelper.NhibernateService.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    session.Update(recipe);
                    transaction.Commit();
                }
            }
        }

        public int getNewID()
        {
            throw new NotImplementedException();
        }

        public Recipe getRecipeById(int id)
        {
            LoadRecipesFromDatabase();
            return _listRecipes.Where(i => i.Id == id).First();
        }

        public Recipe getRecipeByName(string name)
        {
            throw new NotImplementedException();
        }

        public List<Recipe> getRecipes()
        {
            throw new NotImplementedException();
        }

        public List<Recipe> getVisibleRecipes(User user)
        {
            LoadRecipesFromDatabase();
            List<Recipe> result = new List<Recipe>();
            result.AddRange(_listRecipes.Where(i => i.IsPublic == true));
            result.AddRange(_listRecipes.Where(i => i.Username == user.Username).Except(result));
            result.AddRange(_listRecipes.Where(i => i.Username == "Anonimni korisnik").Except(result));
            return result;
        }

        public int GetNewId()
        {
            throw new NotImplementedException();
        }

      
        public void addRecipe(Recipe recipe)
        {
            using (ISession session = NHibernateHelper.NhibernateService.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    session.Save(recipe);
                    transaction.Commit();
                }
            }
        }

        public List<Recipe> getRecipeByIds(List<int> ids)
        {
            List<Recipe> result = new List<Recipe>();
            LoadRecipesFromDatabase();
            foreach(var id in ids)
            {
                result.Add(_listRecipes.Where(i => i.Id == id).First());
            }
            return result;
        }

        private void fillRecipeRepo()
        {
            LoadRecipesFromDatabase();
            if (_listRecipes.Where(i => i.Name == "Bolonjez").Count() == 1)
                return;
            Ingredient meso = new Ingredient("Meso", 450, "g");
            Ingredient meso2 = new Ingredient("Meso", 450, "g");
            Ingredient pasata = new Ingredient("Pasata", 1, "kom");
            Ingredient pasata2 = new Ingredient("Pasata", 1, "kom");
            Ingredient salata = new Ingredient("Salata", 1, "kom");
            Ingredient salata2 = new Ingredient("Salata", 1, "kom");
            Ingredient luk = new Ingredient("Luk", 1, "kom");
            Ingredient luk2 = new Ingredient("Luk", 1, "kom");
            Ingredient luk3 = new Ingredient("Luk", 1, "kom");
            Ingredient luk4 = new Ingredient("Luk", 1, "kom");
            Ingredient luk5 = new Ingredient("Luk", 1, "kom");
            Ingredient luk6 = new Ingredient("Luk", 1, "kom");
            Ingredient luk7 = new Ingredient("Luk", 1, "kom");
            Ingredient tjestenina = new Ingredient("Tjestenina", 500, "g");
            Ingredient tjestenina2 = new Ingredient("Tjestenina", 400, "g");
            Ingredient tjestenina3 = new Ingredient("Tjestenina", 450, "g");
            Ingredient tjestenina4 = new Ingredient("Tjestenina", 450, "g");
            Ingredient sunka = new Ingredient("Šunka", 20, "dg");
            Ingredient krumpir = new Ingredient("Krumpir", 8, "kom");
            Ingredient krumpir2 = new Ingredient("Krumpir", 7, "kom");
            Ingredient krumpir3 = new Ingredient("Krumpir", 6, "kom");
            Ingredient krumpir4 = new Ingredient("Krumpir", 8, "kom");
            Ingredient riza = new Ingredient("Riža", 150, "g");
            Ingredient grasak = new Ingredient("Grašak", 100, "g");
            Ingredient tuna = new Ingredient("Tuna", 1, "kom");
            Ingredient tuna2 = new Ingredient("Tuna", 1, "kom");
            Ingredient povrce = new Ingredient("Mješano povrće", 400, "g");
            Ingredient povrce2 = new Ingredient("Mješano povrće", 400, "g");
            Ingredient povrce3 = new Ingredient("Mješano povrće", 400, "g");
            Ingredient vrhnje = new Ingredient("Vrhnje", 200, "mL");
            Ingredient vrhnje2 = new Ingredient("Vrhnje", 200, "mL");
            Ingredient zobene = new Ingredient("Zobene pahuljice", 50, "g");
            Ingredient voce = new Ingredient("Razno voće", 3, "kom");
            Ingredient kruh = new Ingredient("Kruh", 70, "g");
            Ingredient mlijeko = new Ingredient("Mlijeko", 20, "mL");
            Ingredient mlijeko2 = new Ingredient("Mlijeko", 20, "mL");
            Ingredient mlijeko3 = new Ingredient("Mlijeko", 20, "mL");
            Ingredient med = new Ingredient("Med", 3, "žlica");
            Ingredient cokolino = new Ingredient("Čokolino", 30, "g");
            Ingredient sampinjon = new Ingredient("Šampinjoni", 300, "g");

            List<Ingredient> bolonjez = new List<Ingredient>();
            bolonjez.AddRange(new List<Ingredient> {meso, pasata, luk, tjestenina });
            List<Ingredient> tunaSalata = new List<Ingredient>();
            tunaSalata.AddRange(new List<Ingredient> { tuna, povrce, luk2,salata, tjestenina2 });
            List<Ingredient> mjesanaSalata = new List<Ingredient>();
            mjesanaSalata.AddRange(new List<Ingredient> { povrce, salata2,luk3 });
            List<Ingredient> carbonara = new List<Ingredient>();
            carbonara.AddRange(new List<Ingredient> { vrhnje,tjestenina3, sunka, luk4});
            List<Ingredient> mesoJelo = new List<Ingredient>();
            mesoJelo.AddRange(new List<Ingredient> { meso2});
            List<Ingredient> pasataSTunom = new List<Ingredient>();
            pasataSTunom.AddRange(new List<Ingredient> { pasata2, tuna2, luk5, tjestenina4 });
            List<Ingredient> krumpirList = new List<Ingredient>();
            krumpirList.Add(krumpir);
            List<Ingredient> krumpirList2 = new List<Ingredient>();
            krumpirList2.Add(krumpir);
            List<Ingredient> restani = new List<Ingredient>();
            restani.AddRange(new List<Ingredient> { krumpir, luk6 });
            List<Ingredient> riziBizi = new List<Ingredient>();
            riziBizi.AddRange(new List<Ingredient> { riza, grasak});

            List<Recipe> defaultRecipes = new List<Recipe>();

            defaultRecipes.Add(new Recipe("Bolonjez", "Prije svega povrće (luk, češnjak, mrkvu i celerovu stabljiku) i mesnatu slaninu sitno nasjeckamo. U lonac ulijemo dvije žlice maslinovog ulja, dodamo maslac, nasjeckano povrće, nasjeckanu slaninu, malo posolimo, izmiješamo sastojke, na laganoj vatri pržimo dok povrće ne omekša, te ga izvadimo na stranu.Zatim u lonac po potrebi ulijemo još malo ulja, dodamo miješano mljeveno meso, te ga zajedno s povrćem pržimo dok me ispari tekućina što ju je meso ispustilo i dok meso ne postane zrnasto.Potom u lonac ulijemo crno vino i pričekamo da tekućina na pola ispari.Zatim sastojke začinimo solju i paprom, umiješamo koncentrat rajčice, dodamo nasjeckanu rajčicu pelati, sjeckani peršin, šećer, sušeni mažuran, malo muškatnog oraščića, mesni temeljac, te sve zajedno izmiješamo i nastavimo kuhati na laganoj vatri po prilici sat vremena.Gotov umak poslužimo s kuhanim špagetima.",
                65, 4, bolonjez, "Glavno jelo s mesom", true, "Anonymus"));
            defaultRecipes.Add(new Recipe("Tuna salata", "Filete tune narežite na manje komadiće pa ih pomiješajte s narezanim kuhanim jajima, krastavcima i vlascem. Dodajte još protisnuti češnjak, maslinovo ulje i jabučni ocat. Salatu lagano izmiješajte i ohladite. Poslužite kao hladno predjelo ili mali samostalni obrok.Salati možete dodati kapare i 2-3 žlice kiselog vrhnja.",
                35, 3, tunaSalata, "Vegetarijansko glavno jelo", true, "Anonymus"));
            defaultRecipes.Add( new Recipe("Pomfrit", "Zagrijte pećnicu na 200°C.Krumpir narežite na duguljaste kriške.Pomiješajte maslinovo ulje, mljevenu crvenu papriku, češnjak u prahu, chili u prahu i luk u prahu. Premažite krumpiriće dobivenom smjesom i posložite ih na lim za pečenje.Pecite 40-45 minuta u prethodno zagrijanoj pećnici.",
               30, 3, krumpirList, "Prilog", true, "Anonymus"));
            defaultRecipes.Add( new Recipe("Kuhani krumpir", "Skuhati krumpir u ljusci , kad je krumpir kuhan i kad se ohladi, oguliti ga i narezati na kriške ili kockice. Na sitno narezati luk i poprziti na vodi - dodati kuhani krumpir i peršin i sve zajedno dinsati nekih 5 minuta na vatri. Dodati bućinog ulja i malo soli. Krumpir može ostati narezan na kockice ili ga možete zdrobiti (restani/tenfani ) svakome po želji . ",
               35, 3, krumpirList2, "Prilog", true, "Anonymus"));
            defaultRecipes.Add(new Recipe("Restani krumpir", "Izaberite krumpire podjednake veličine, operite ih i stavite u posudu s vodom. Krumpir kuhajte 45 minuta. Kako biste bili sigurni da je krumpir kuhan, probodite ga drvenim štapićem. Kuhani krumpir ocijedite, rashladite pod mlazom vode i ogulite. U posudi na zagrijanom ulju popecite sitno nasjeckani luk na kockice, a zatim dodajte krumpir koji ste narezali na tanke ploške. Po želji, dodajte sol i papar, promiješajte i kratko zagrijte. Poslužite kao prilog uz pečena mesa.Krumpiru možete dodati nasjeckani peršinov list ili vlasac.",
               40, 4, restani, "Prilog", true, "Anonymus"));
            defaultRecipes.Add( new Recipe("Carbonara", "Tjesteninu skuhati al dente.Na malo maslinovog ulja pržiti sitno sjeckanu slaninu.U posebnoj zdjelici umutiti jaja sa parmezanom.Kuhanu tjesteninu istresti na prženu slaninu, dodati vrhnje za kuhanje, dobro promiješati pa kada postane ravnomjerno vruće i kada tjestenina nakon 2-3 minute upije tekućinu iz vrhnja, dodati jaja sa parmezanom. Po želji začiniti solju, paprom ili vegetom. Dobro promiješati kako bi se jaja ravnomjerno rasporedila i sir otopio.Poslužiti uz salatu po želji i čašu orošenog bijelog vina!",
               45, 4, carbonara, "Glavno jelo s mesom", true, "Anonymus"));
            defaultRecipes.Add( new Recipe("Rizi bizi", "Na ulju kratko popržiti narezani luk, dodati mrkvicu i grašak pa uz povremeno miješanje pirjati 15tak minuta.Začiniti, dodati malo vode tek toliko da pokrije povrće pa pirjati još 15tak min. (ako se rizi-bizi kuha sa svježim graškom bit će ga potrebno kuhati nešto duže)Dodati rižu, promiješati, kratko propirjati pa naliti svu vodu.Poklopiti pa na laganoj vatri pirjati oko 20 min. (miješanje nije potrebno)Kad je riža mekana, promiješati, po potrebi dosoliti (jer riža upije dosta soli), doliti još pola šalice vode, promiješati i poklopljeno pustiti da odstoji do posluživanja a najmanje 20 minuta .",
               25, 4, riziBizi, "Prilog", true, "Anonymus"));
            defaultRecipes.Add( new Recipe("Mješana salata", "Očišćeno i oprano povrće dobro ocijediti i naribati. Umiješati sol. Posebno kratko prokuhati ocat i vodu, te vruće preliti preko naribanog povrća. Puniti tegle i zatvoriti.",
               20, 3, mjesanaSalata, "Salata", true, "Anonymus"));
            defaultRecipes.Add(new Recipe("Zobene pahuljice s voćem", "Dodati vruće mlijeko u zobene pahuljice i pustiti da stoji 5 minuta. Dodati voće i orašaste plodove po želji",
                10, 1, new List<Ingredient> { zobene, voce, mlijeko }, "Međuobrok", true, "Anonymus"));
            defaultRecipes.Add(new Recipe("Kruh s medom", "Na nekoliko kriški kruha namazati med, nije komlicirano :)",
                 10, 1, new List<Ingredient> { kruh, med}, "Međuobrok", true, "Anonymus"));
            defaultRecipes.Add(new Recipe("Čokolino", "U zdijelicu s mlijekom dodati 30 grama čokolina i promiješati",
                10, 1, new List<Ingredient> { cokolino, mlijeko }, "Međuobrok", true, "Anonymus"));
            defaultRecipes.Add(new Recipe("Pljeskavice", "Mlijeveno meso oblikovati u kuglice pa ih rukom sploštiti. U tavu staviti malo ulja i peći s povremenim okretanjem pljeskavice na drugu stranu. Peći dok ne postignu željenu boju na umjerenoj vatri",
                30, 4, new List<Ingredient> { meso, luk7}, "Glavno jelo s mesom", true, "Anonymus"));
            defaultRecipes.Add(new Recipe("Juha od šaminjona", "Izrezati šampinjone na tanke listiće, krumpire također, a pancetu na vrlo sitne kockice. Za to vrijeme otopiti maslac, dodati koju kap ulja i na tome popržiti pancetu, pa prvo staviti sjeckane šampinjone da puste tekućinu.Potom dodati krumpir i protisnuti češnjak, i sve skupa malo prodinstati dok tekućina ne ispari. Kad se počne primati za dno, podliti vodom i staviti da lagano kuha. U zdjelici otopiti škrobno brašno s malo vode i kiselog vrhnja. Kad krumpir i šampinjoni omekšaju, izvaditi oko 2/3 žlicom za cijeđenje i prebaciti u visoku posudu te usitniti štapnim mikserom. Dobivenu smjesu vratiti u posudu s ostatkom šampinjona, promiješati i dodati škrobno brašno s vrhnjem.",
                35, 4, new List<Ingredient> { sampinjon, mlijeko2, vrhnje2 }, "Juha", true, "Anonymus"));
            defaultRecipes.Add(new Recipe("Povrtna juha", "Željeno povrće nasjeckati i kuhati 45 minuta ili dok ne postane mekano",
                35, 4, new List<Ingredient> { povrce3 }, "Juha", true, "Anonymus"));

            foreach (var rec in defaultRecipes)
                addRecipe(rec);

            LoadRecipesFromDatabase();

        }
    }

}
