﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MealPlanner2.Model.Repositories;
using MealPlanner2.Model;
using NHibernate;

namespace MealPlanner2.DALNHibernate.Repositories
{
    public  class NHShoppingListRepository : IShoppingListRepository
    {
        private static NHShoppingListRepository _nhShoppingListRepository;
        private IList<ShoppingList> _listShoppingLists = new List<ShoppingList>();

        public static NHShoppingListRepository GetInstance()
        {
            if (_nhShoppingListRepository == null)
            {
                _nhShoppingListRepository = new NHShoppingListRepository();
                //_nhShoppingListRepository.FillRepo();
            }

            return _nhShoppingListRepository;
        }
        private void LoadShoppingListsFromDatabase()
        {
            using (ISession session = NHibernateHelper.NhibernateService.OpenSession())
            {
                IQuery query = session.CreateQuery(
                    "from ShoppingList as list order by list.ModifiedAt desc");
                _listShoppingLists = query.List<ShoppingList>();
            }


        }
       
        public void addShoppingList(ShoppingList ShoppingList)
        {
            using (ISession session = NHibernateHelper.NhibernateService.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    session.Save(ShoppingList);
                    transaction.Commit();
                }
            }
        }

        public void deleteShoppingList(ShoppingList list)
        {
            using (ISession session = NHibernateHelper.NhibernateService.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {


                    session.Delete(list);
                    transaction.Commit();
                }
            }
        }

        public void deleteShoppingList(int id)
        {
            throw new NotImplementedException();
        }

        public void editShoppingList(ShoppingList list)
        {
            using (ISession session = NHibernateHelper.NhibernateService.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                   

                    session.Update(list);
                    transaction.Commit();
                }
            }
        }

        

        public List<ShoppingList> getMyShoppingLists(User user)
        {
            LoadShoppingListsFromDatabase();
            return _listShoppingLists.Where(i => i.Username == user.Username).ToList();
        }

        public int getNewID()
        {
            throw new NotImplementedException();
        }

        public ShoppingList getShoppingListById(int id)
        {
            return _listShoppingLists.Where(i => i.Id == id).FirstOrDefault();
        }

        public List<ShoppingList> getShoppingLists()
        {
            throw new NotImplementedException();
        }
    }
}
