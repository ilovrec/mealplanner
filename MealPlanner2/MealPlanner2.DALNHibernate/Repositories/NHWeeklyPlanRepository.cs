﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MealPlanner2.Model.Repositories;
using MealPlanner2.Model;
using NHibernate;

namespace MealPlanner2.DALNHibernate.Repositories
{
    public class NHWeeklyPlanRepository: IWeeklyPlanRepository
    {
        private static NHWeeklyPlanRepository _nhWeeklyPlanRepository;
        private IList<WeeklyPlan> _listWeeklyPlans = new List<WeeklyPlan>();

        public static NHWeeklyPlanRepository GetInstance()
        {
            if (_nhWeeklyPlanRepository == null)
            {
                _nhWeeklyPlanRepository = new NHWeeklyPlanRepository();
                //_nhWeeklyPlanRepository.FillRepo();
            }

            return _nhWeeklyPlanRepository;
        }
        private void LoadWeeklyPlansFromDatabase()
        {
            using (ISession session = NHibernateHelper.NhibernateService.OpenSession())
            {
                IQuery query = session.CreateQuery(
                    "from WeeklyPlan as rec order by rec.Monday desc");
                _listWeeklyPlans = query.List<WeeklyPlan>();
            }


        }



        public void addWeeklyPlan(WeeklyPlan WeeklyPlan)
        {
            LoadWeeklyPlansFromDatabase();
            if (_listWeeklyPlans.Where(i => i.Monday.Date == WeeklyPlan.Monday.Date && i.Username==WeeklyPlan.Username).Count() > 0)
                throw new NonUniqueObjectException("Postoji tjedni plan za taj datum, ", WeeklyPlan.Id, "weeklyPlan: "+WeeklyPlan.Monday.Date.ToString());

            using (ISession session = NHibernateHelper.NhibernateService.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    session.Save(WeeklyPlan);
                    transaction.Commit();
                }
            }
        }

        public void deleteWeeklyPlan(WeeklyPlan WeeklyPlan)
        {
            using (ISession session = NHibernateHelper.NhibernateService.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    session.Delete(WeeklyPlan);
                    transaction.Commit();
                }
            }
        }

        public void editWeeklyPlan(WeeklyPlan newPlan)
        {
            using (ISession session = NHibernateHelper.NhibernateService.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                  
                    session.Update(newPlan);
                    transaction.Commit();
                }
            }
        }

        public void updateDailyPlan(DailyPlan plan)
        {
            using (ISession session = NHibernateHelper.NhibernateService.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    session.Update(plan);
                    transaction.Commit();
                }
            }
        }

        

        public List<WeeklyPlan> getWeeklyPlans()
        {
            throw new NotImplementedException();
        }

        public List<WeeklyPlan> getVisibleWeeklyPlans(User user)
        {
            LoadWeeklyPlansFromDatabase();
            return _listWeeklyPlans.Where(i => i.Username == user.Username).ToList();

        }

        public WeeklyPlan getWeeklyPlanById(int id)
        {
            LoadWeeklyPlansFromDatabase();
            return _listWeeklyPlans.Where(i => i.Id == id).FirstOrDefault();
        }

        public int getNewID()
        {
            throw new NotImplementedException();
        }

        public void deleteWeeklyPlan(int id)
        {
            throw new NotImplementedException();
        }
    }
}
