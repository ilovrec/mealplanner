﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MealPlanner2.Controllers;
using MealPlanner2.Presentation;
using MealPlanner2.MemoryBasedDAL;
using MealPlanner2.DALNHibernate.NHibernateHelper;
using MealPlanner2.DALNHibernate.Repositories;


namespace MealPlanner2.AppStarter
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            NhibernateService.OpenSessionFactory();
            WindowsFormsFactory _formsFactory = new WindowsFormsFactory();
            NHRecipeRepository recipeRepository = NHRecipeRepository.GetInstance();
            NHUserRepository userRepo = NHUserRepository.getInstance();
            NHShoppingListRepository shopListRepo = NHShoppingListRepository.GetInstance();
            NHWeeklyPlanRepository weeklyRepo = NHWeeklyPlanRepository.GetInstance();

            // umjesto Singleton patterna, imati ćemo samo jednu instancu repozitorija unutar glavnog programa
            //TransactionRepository _transRepo = new TransactionRepository();

            // a za Account repozitorij koristimo Singleton
            MainController mainController = new MainController(_formsFactory, recipeRepository,
                            shopListRepo, weeklyRepo, userRepo);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MealPlanner2.Presentation.FormMain(mainController));
        }
    }
}
