# MealPlanner

This is a Windows forms app for easier planning of meals.

## Description
User can use this app to publish and read recipes. They can also store recipes which are private. Users can make weekly meal plans with three meals for each day.The app also offers the possibility of saving multiple shopping lists to help the user plan their grocery store trip.


## Arhitecture
App is designed using MVP pattern with Windows forms as presentation layer and 
NHibernate as database. 
